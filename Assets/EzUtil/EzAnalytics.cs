﻿
public class EzAnalytics {

    public static void LogScreen(string title) {
        GoogleAnalyticsV4.instance.LogScreen(title);
    }

	public static void LogEvent(string category, string action, string label = "", long value = 0) {
        GoogleAnalyticsV4.instance.LogEvent(category, action, label, value);
    }
}
