﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBorder2D : MonoBehaviour {

    public BoxCollider2D left;
    public BoxCollider2D right;
    public BoxCollider2D bottom;
    public BoxCollider2D top;

    public float marginLeft;
    public float marginRight;
    public float marginBottom;
    public float marginTop;

    // Use this for initialization
    void Start() {
        Rect rect = Camera.main.GetScreenWorldRect();
        if (left != null) {
            left.transform.position = left.transform.position.NewX(rect.xMin - left.size.x / 2 + marginLeft);
        }
        if (right != null) {
            right.transform.position = left.transform.position.NewX(rect.xMax + right.size.x / 2 - marginRight);
        }
        if (bottom != null) {
            bottom.transform.position = bottom.transform.position.NewY(rect.yMin - bottom.size.y / 2 + marginBottom);
        }
        if (bottom != null) {
            top.transform.position = top.transform.position.NewY(rect.yMax + top.size.y / 2 - marginTop);
        }
	}
}
