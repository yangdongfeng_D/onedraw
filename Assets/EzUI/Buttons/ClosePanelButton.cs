﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClosePanelButton : MonoBehaviour {

    public string panelName;

    // Use this for initialization
    void Start() {
        GetComponent<Button>().onClick.AddListener(() => {
            if (!string.IsNullOrEmpty(panelName)) {
                Panel.Close(panelName);
            }
        });
    }

    // Update is called once per frame
    void Update() {

    }
}
