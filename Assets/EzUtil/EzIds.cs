﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzIds {

    public static string bundleID { 
        get {
#if UNITY_5_6 || UNITY_2017
            return Application.identifier;
#else
            return Application.bundleIdentifier;
#endif
        }
    }

	public static string deviceID {
        get {
            if (_deviceID == null) {
                _deviceID = EzDevice.GetUniqueIDInMD5();
            }
            return _deviceID;
        }
    }
    private static string _deviceID;

    public static string appID {
        get {
            if (_appID == null) {
                _appID = EzCrypto.MD5(bundleID);
            }
            return _appID;
        }
    }
    private static string _appID;

    public static string appIDLocalized {
        get {
            if (_appIDLocalized == null) {
                _appIDLocalized = EzCrypto.MD5(Application.companyName + Application.productName + bundleID);
            }
            return _appIDLocalized;
        }
    }
    private static string _appIDLocalized;

    public static string versionID {
        get {
            if (_versionID == null) {
                _versionID = EzCrypto.MD5(bundleID + Application.version);
            }
            return _versionID;
        }
    }
    private static string _versionID;
}
