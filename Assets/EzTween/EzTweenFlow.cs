using UnityEngine;
using System.Collections;


/// <summary>
/// TweenFlows are used for creating a chain of Tweens via the append/prepend methods. You can also get timeline
/// like control by inserting Tweens and setting them to start at a specific time. Note that TweenFlows do not
/// honor the delays set within regular Tweens. Use the append/prependDelay method to add any required delays
/// </summary>
public class EzTweenFlow : EzTweenCollection {
	public EzTweenFlow() : this(new EzTweenCollectionConfig()) {
	}

	public EzTweenFlow(EzTweenCollectionConfig config) : base(config) {
	}


	#region internal Flow management

	/// <summary>
	/// the item being added already has a start time so no extra parameter is needed
	/// </summary>
	private void Insert(TweenFlowItem item) {
		// early out for invalid items
		if (item.tween != null && !item.tween.IsValid())
			return;
		
		if (float.IsInfinity(item.duration)) {
			Debug.LogError("adding a Tween with infinite iterations to a TweenFlow is not permitted");
			return;
		}
		
		if (item.tween != null) {
			if (item.tween.reversed != reversed) {
				Debug.LogError("adding a Tween that doesn't match the isReversed property of the TweenFlow is not permitted.");
				return;
			}

			// ensure the tween isnt already live
			EzTweenManager.RemoveTween(item.tween);

			// ensure that the item is marked to play.
			item.tween.Play();
		}

		// add the item then sort based on startTimes
		flowItems.Add(item);
		flowItems.Sort(( x, y) => {
			return x.startTime.CompareTo(y.startTime);
		});
		
		duration = Mathf.Max(item.startTime + item.duration, duration);

		if (loops < 0)
			totalDuration = float.PositiveInfinity;
		else
			totalDuration = duration * loops;
	}

	#endregion

	
	#region Flow management

	/// <summary>
	/// inserts a Tween and sets it to start at the given startTime
	/// </summary>
	public EzTweenFlow Insert(float startTime, EzTweenBase tween) {
		var item = new TweenFlowItem(startTime, tween);
		Insert(item);
		
		return this;
	}

	#endregion
	

}
