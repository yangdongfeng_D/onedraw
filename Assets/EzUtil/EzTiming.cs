﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_5_5_OR_NEWER
using UnityEngine.Profiling;
#endif

public class EzTiming : MonoBehaviour {
    public enum DebugInfoType {
        None,
        SeperateCoroutines,
        SeperateTags
    }

    /// <summary>
    /// The time between calls to SlowUpdate.
    /// </summary>
    [Tooltip("How quickly the SlowUpdate segment ticks.")]
    public float TimeBetweenSlowUpdateCalls = 1f / 7f;
    /// <summary>
    /// The amount that each coroutine should be seperated inside the Unity profiler. NOTE: When the profiler window
    /// is not open this value is ignored and all coroutines behave as if "None" is selected.
    /// </summary>
    [Tooltip("How much data should be sent to the profiler window when it's open.")]
    public DebugInfoType ProfilerDebugAmount = DebugInfoType.SeperateCoroutines;
    /// <summary>
    /// Whether the manual timeframe should automatically trigger during the update segment.
    /// </summary>
    [Tooltip("When using manual timeframe, should it run automatically after the update loop or only when TriggerManualTimframeUpdate is called.")]
    public bool AutoTriggerManualTimeframe = true;
    /// <summary>
    /// The number of coroutines that are being run in the Update segment.
    /// </summary>
    [Tooltip("A count of the number of Update coroutines that are currently running."), Space(12)]
    public int UpdateCoroutines;
    /// <summary>
    /// The number of coroutines that are being run in the FixedUpdate segment.
    /// </summary>
    [Tooltip("A count of the number of FixedUpdate coroutines that are currently running.")]
    public int FixedUpdateCoroutines;
    /// <summary>
    /// The number of coroutines that are being run in the LateUpdate segment.
    /// </summary>
    [Tooltip("A count of the number of LateUpdate coroutines that are currently running.")]
    public int LateUpdateCoroutines;
    /// <summary>
    /// The number of coroutines that are being run in the SlowUpdate segment.
    /// </summary>
    [Tooltip("A count of the number of SlowUpdate coroutines that are currently running.")]
    public int SlowUpdateCoroutines;
    /// <summary>
    /// The time in seconds that the current segment has been running.
    /// </summary>
    [HideInInspector]
    public double localTime;
    /// <summary>
    /// The time in seconds that the current segment has been running.
    /// </summary>
    public static float LocalTime { get { return (float)Instance.localTime; } }
    /// <summary>
    /// The amount of time in fractional seconds that elapsed between this frame and the last frame.
    /// </summary>
    [HideInInspector]
    public float deltaTime;
    /// <summary>
    /// The amount of time in fractional seconds that elapsed between this frame and the last frame.
    /// </summary>
    public static float DeltaTime { get { return Instance.deltaTime; } }
    /// <summary>
    /// When defined, all errors from inside coroutines will be passed into this function instead of falling through to the Unity console.
    /// </summary>
    public System.Action<System.Exception> OnError;
    /// <summary>
    /// Used for advanced coroutine control.
    /// </summary>
    public static System.Func<IEnumerator<float>, CoroutineHandle, IEnumerator<float>> ReplacementFunction;
    /// <summary>
    /// This event fires just before each segment is run.
    /// </summary>
    public static event System.Action OnPreExecute;
    /// <summary>
    /// You can use "yield return Timing.WaitForOneFrame;" inside a coroutine function to go to the next frame. 
    /// This is equalivant to "yeild return 0f;"
    /// </summary>
    public readonly static float WaitForOneFrame = 0f;
    /// <summary>
    /// The main thread that (almost) everything in unity runs in.
    /// </summary>
    public static System.Threading.Thread MainThread { get; private set; }

    private bool _runningUpdate;
    private bool _runningLateUpdate;
    private bool _runningFixedUpdate;
    private bool _runningSlowUpdate;
    private int _currentUpdateFrame;
    private int _currentFixedUpdateFrame;
    private int _currentSlowUpdateFrame;
    private int _nextUpdateProcessSlot;
    private int _nextLateUpdateProcessSlot;
    private int _nextFixedUpdateProcessSlot;
    private int _nextSlowUpdateProcessSlot;
    private double _lastUpdateTime;
    private double _lastFixedUpdateTime;
    private double _lastSlowUpdateTime;
    private float _lastSlowUpdateDeltaTime;
    private ushort _framesSinceUpdate;
    private ushort _expansions = 1;
    private byte _instanceID;

    private readonly Dictionary<CoroutineHandle, HashSet<ProcessData>> _waitingTriggers = new Dictionary<CoroutineHandle, HashSet<ProcessData>>();
    private readonly Queue<System.Exception> _exceptions = new Queue<System.Exception>();
    private readonly Dictionary<CoroutineHandle, ProcessIndex> _handleToIndex = new Dictionary<CoroutineHandle, ProcessIndex>();
    private readonly Dictionary<ProcessIndex, CoroutineHandle> _indexToHandle = new Dictionary<ProcessIndex, CoroutineHandle>();
    private readonly Dictionary<ProcessIndex, string> _processTags = new Dictionary<ProcessIndex, string>();
    private readonly Dictionary<string, HashSet<ProcessIndex>> _taggedProcesses = new Dictionary<string, HashSet<ProcessIndex>>();

    private IEnumerator<float>[] UpdateProcesses = new IEnumerator<float>[InitialBufferSizeLarge];
    private IEnumerator<float>[] LateUpdateProcesses = new IEnumerator<float>[InitialBufferSizeSmall];
    private IEnumerator<float>[] FixedUpdateProcesses = new IEnumerator<float>[InitialBufferSizeMedium];
    private IEnumerator<float>[] SlowUpdateProcesses = new IEnumerator<float>[InitialBufferSizeMedium];
    private bool[] UpdatePaused = new bool[InitialBufferSizeLarge];
    private bool[] LateUpdatePaused = new bool[InitialBufferSizeSmall];
    private bool[] FixedUpdatePaused = new bool[InitialBufferSizeMedium];
    private bool[] SlowUpdatePaused = new bool[InitialBufferSizeMedium];

    private const ushort FramesUntilMaintenance = 64;
    private const int ProcessArrayChunkSize = 64;
    private const int InitialBufferSizeLarge = 256;
    private const int InitialBufferSizeMedium = 64;
    private const int InitialBufferSizeSmall = 8;

    private static readonly Dictionary<byte, EzTiming> ActiveInstances = new Dictionary<byte, EzTiming>();
    private static EzTiming _instance;

    public static EzTiming Instance {
        get {
            if (_instance == null || !_instance.gameObject) {
                GameObject instanceHome = GameObject.Find("EzTiming");
                if (instanceHome == null) {
                    instanceHome = new GameObject("EzTiming");
                    DontDestroyOnLoad(instanceHome);
                    _instance = instanceHome.AddComponent<EzTiming>();
                } else {
                    _instance = instanceHome.GetComponent<EzTiming>() ?? instanceHome.AddComponent<EzTiming>();
                }
            }
            return _instance;
        }

        set { _instance = value; }
    }

    void Awake() {
        if (_instance == null)
            _instance = this;
        else
            deltaTime = _instance.deltaTime;

        _instanceID = 0x01;
        while (ActiveInstances.ContainsKey(_instanceID))
            _instanceID++;

        if (_instanceID == 0x20) {
            GameObject.Destroy(gameObject);
            throw new System.OverflowException("You are only allowed 31 instances of MEC at one time.");
        }

        ActiveInstances.Add(_instanceID, this);

        if (MainThread == null)
            MainThread = System.Threading.Thread.CurrentThread;
    }

    void OnDestroy() {
        if (_instance == this)
            _instance = null;

        ActiveInstances.Remove(_instanceID);
    }

    void Update() {
        if (OnPreExecute != null)
            OnPreExecute();

        if (_lastSlowUpdateTime + TimeBetweenSlowUpdateCalls < Time.realtimeSinceStartup && _nextSlowUpdateProcessSlot > 0) {
            ProcessIndex coindex = new ProcessIndex { seg = Segment.SlowUpdate };
            _runningSlowUpdate = true;
            UpdateTimeValues(coindex.seg);

            for (coindex.idx = 0; coindex.idx < _nextSlowUpdateProcessSlot; coindex.idx++) {
                if (!SlowUpdatePaused[coindex.idx] && SlowUpdateProcesses[coindex.idx] != null && !(localTime < SlowUpdateProcesses[coindex.idx].Current)) {
                    if (ProfilerDebugAmount != DebugInfoType.None) {
                        Profiler.BeginSample(ProfilerDebugAmount == DebugInfoType.SeperateTags
                                                 ? ("Processing Coroutine (Slow Update)" +
                                                    (_processTags.ContainsKey(coindex) ? ", tag " + _processTags[coindex] : ", no tag"))
                                                 : "Processing Coroutine (Slow Update)");
                    }

                    try {
                        if (!SlowUpdateProcesses[coindex.idx].MoveNext()) {
                            SlowUpdateProcesses[coindex.idx] = null;
                        } else if (SlowUpdateProcesses[coindex.idx] != null && float.IsNaN(SlowUpdateProcesses[coindex.idx].Current)) {
                            if (ReplacementFunction == null) {
                                SlowUpdateProcesses[coindex.idx] = null;
                            } else {
                                SlowUpdateProcesses[coindex.idx] = ReplacementFunction(SlowUpdateProcesses[coindex.idx], _indexToHandle[coindex]);

                                ReplacementFunction = null;
                                coindex.idx--;
                            }
                        }
                    } catch (System.Exception ex) {
                        if (OnError == null)
                            _exceptions.Enqueue(ex);
                        else
                            OnError(ex);

                        SlowUpdateProcesses[coindex.idx] = null;
                    }

                    if (ProfilerDebugAmount != DebugInfoType.None)
                        Profiler.EndSample();
                }
            }

            _runningSlowUpdate = false;
        }

        if (_nextUpdateProcessSlot > 0) {
            ProcessIndex coindex = new ProcessIndex { seg = Segment.Update };
            _runningUpdate = true;
            UpdateTimeValues(coindex.seg);

            for (coindex.idx = 0; coindex.idx < _nextUpdateProcessSlot; coindex.idx++) {
                if (!UpdatePaused[coindex.idx] && UpdateProcesses[coindex.idx] != null && !(localTime < UpdateProcesses[coindex.idx].Current)) {
                    if (ProfilerDebugAmount != DebugInfoType.None) {
                        Profiler.BeginSample(ProfilerDebugAmount == DebugInfoType.SeperateTags
                                                 ? ("Processing Coroutine" +
                                                    (_processTags.ContainsKey(coindex) ? ", tag " + _processTags[coindex] : ", no tag"))
                                                 : "Processing Coroutine");
                    }

                    try {
                        if (!UpdateProcesses[coindex.idx].MoveNext()) {
                            UpdateProcesses[coindex.idx] = null;
                        } else if (UpdateProcesses[coindex.idx] != null && float.IsNaN(UpdateProcesses[coindex.idx].Current)) {
                            if (ReplacementFunction == null) {
                                UpdateProcesses[coindex.idx] = null;
                            } else {
                                UpdateProcesses[coindex.idx] = ReplacementFunction(UpdateProcesses[coindex.idx], _indexToHandle[coindex]);

                                ReplacementFunction = null;
                                coindex.idx--;
                            }
                        }
                    } catch (System.Exception ex) {
                        if (OnError == null)
                            _exceptions.Enqueue(ex);
                        else
                            OnError(ex);

                        UpdateProcesses[coindex.idx] = null;
                    }

                    if (ProfilerDebugAmount != DebugInfoType.None)
                        Profiler.EndSample();
                }
            }

            _runningUpdate = false;
        }

        if (++_framesSinceUpdate > FramesUntilMaintenance) {
            _framesSinceUpdate = 0;

            if (ProfilerDebugAmount != DebugInfoType.None)
                Profiler.BeginSample("Maintenance Task");

            RemoveUnused();

            if (ProfilerDebugAmount != DebugInfoType.None)
                Profiler.EndSample();
        }

        if (_exceptions.Count > 0)
            throw _exceptions.Dequeue();
    }

    void FixedUpdate() {
        if (OnPreExecute != null)
            OnPreExecute();

        if (_nextFixedUpdateProcessSlot > 0) {
            ProcessIndex coindex = new ProcessIndex { seg = Segment.FixedUpdate };
            _runningFixedUpdate = true;
            UpdateTimeValues(coindex.seg);

            for (coindex.idx = 0; coindex.idx < _nextFixedUpdateProcessSlot; coindex.idx++) {
                if (!FixedUpdatePaused[coindex.idx] && FixedUpdateProcesses[coindex.idx] != null && !(localTime < FixedUpdateProcesses[coindex.idx].Current)) {
                    if (ProfilerDebugAmount != DebugInfoType.None) {
                        Profiler.BeginSample(ProfilerDebugAmount == DebugInfoType.SeperateTags
                                                 ? ("Processing Coroutine" +
                                                    (_processTags.ContainsKey(coindex) ? ", tag " + _processTags[coindex] : ", no tag"))
                                                 : "Processing Coroutine");
                    }

                    try {
                        if (!FixedUpdateProcesses[coindex.idx].MoveNext()) {
                            FixedUpdateProcesses[coindex.idx] = null;
                        } else if (FixedUpdateProcesses[coindex.idx] != null && float.IsNaN(FixedUpdateProcesses[coindex.idx].Current)) {
                            if (ReplacementFunction == null) {
                                FixedUpdateProcesses[coindex.idx] = null;
                            } else {
                                FixedUpdateProcesses[coindex.idx] = ReplacementFunction(FixedUpdateProcesses[coindex.idx], _indexToHandle[coindex]);

                                ReplacementFunction = null;
                                coindex.idx--;
                            }
                        }
                    } catch (System.Exception ex) {
                        if (OnError == null)
                            _exceptions.Enqueue(ex);
                        else
                            OnError(ex);

                        FixedUpdateProcesses[coindex.idx] = null;
                    }

                    if (ProfilerDebugAmount != DebugInfoType.None)
                        Profiler.EndSample();
                }
            }

            _runningFixedUpdate = false;
        }

        if (_exceptions.Count > 0)
            throw _exceptions.Dequeue();
    }

    void LateUpdate() {
        if (OnPreExecute != null)
            OnPreExecute();

        if (_nextLateUpdateProcessSlot > 0) {
            ProcessIndex coindex = new ProcessIndex { seg = Segment.LateUpdate };
            _runningLateUpdate = true;
            UpdateTimeValues(coindex.seg);

            for (coindex.idx = 0; coindex.idx < _nextLateUpdateProcessSlot; coindex.idx++) {
                if (!LateUpdatePaused[coindex.idx] && LateUpdateProcesses[coindex.idx] != null && !(localTime < LateUpdateProcesses[coindex.idx].Current)) {
                    if (ProfilerDebugAmount != DebugInfoType.None) {
                        Profiler.BeginSample(ProfilerDebugAmount == DebugInfoType.SeperateTags
                                                 ? ("Processing Coroutine" +
                                                    (_processTags.ContainsKey(coindex) ? ", tag " + _processTags[coindex] : ", no tag"))
                                                 : "Processing Coroutine");
                    }

                    try {
                        if (!LateUpdateProcesses[coindex.idx].MoveNext()) {
                            LateUpdateProcesses[coindex.idx] = null;
                        } else if (LateUpdateProcesses[coindex.idx] != null && float.IsNaN(LateUpdateProcesses[coindex.idx].Current)) {
                            if (ReplacementFunction == null) {
                                LateUpdateProcesses[coindex.idx] = null;
                            } else {
                                LateUpdateProcesses[coindex.idx] = ReplacementFunction(LateUpdateProcesses[coindex.idx], _indexToHandle[coindex]);

                                ReplacementFunction = null;
                                coindex.idx--;
                            }
                        }
                    } catch (System.Exception ex) {
                        if (OnError == null)
                            _exceptions.Enqueue(ex);
                        else
                            OnError(ex);

                        LateUpdateProcesses[coindex.idx] = null;
                    }

                    if (ProfilerDebugAmount != DebugInfoType.None)
                        Profiler.EndSample();
                }
            }

            _runningLateUpdate = false;
        }

        if (_exceptions.Count > 0)
            throw _exceptions.Dequeue();
    }

    private void RemoveUnused() {
        var waitTrigsEnum = _waitingTriggers.GetEnumerator();
        while (waitTrigsEnum.MoveNext()) {
            if (waitTrigsEnum.Current.Value.Count == 0) {
                _waitingTriggers.Remove(waitTrigsEnum.Current.Key);
                waitTrigsEnum = _waitingTriggers.GetEnumerator();
                continue;
            }

            if (_handleToIndex.ContainsKey(waitTrigsEnum.Current.Key) && CoindexIsNull(_handleToIndex[waitTrigsEnum.Current.Key])) {
                CloseWaitingProcess(waitTrigsEnum.Current.Key);
                waitTrigsEnum = _waitingTriggers.GetEnumerator();
            }
        }

        ProcessIndex outer, inner;
        outer.seg = inner.seg = Segment.Update;
        for (outer.idx = inner.idx = 0; outer.idx < _nextUpdateProcessSlot; outer.idx++) {
            if (UpdateProcesses[outer.idx] != null) {
                if (outer.idx != inner.idx) {
                    UpdateProcesses[inner.idx] = UpdateProcesses[outer.idx];
                    UpdatePaused[inner.idx] = UpdatePaused[outer.idx];
                    MoveTag(outer, inner);

                    if (_indexToHandle.ContainsKey(inner)) {
                        _handleToIndex.Remove(_indexToHandle[inner]);
                        _indexToHandle.Remove(inner);
                    }

                    _handleToIndex[_indexToHandle[outer]] = inner;
                    _indexToHandle.Add(inner, _indexToHandle[outer]);
                    _indexToHandle.Remove(outer);
                }
                inner.idx++;
            }
        }
        for (outer.idx = inner.idx; outer.idx < _nextUpdateProcessSlot; outer.idx++) {
            UpdateProcesses[outer.idx] = null;
            UpdatePaused[outer.idx] = false;
            RemoveTag(outer);

            if (_indexToHandle.ContainsKey(outer)) {
                _handleToIndex.Remove(_indexToHandle[outer]);
                _indexToHandle.Remove(outer);
            }
        }

        UpdateCoroutines = _nextUpdateProcessSlot = inner.idx;

        outer.seg = inner.seg = Segment.FixedUpdate;
        for (outer.idx = inner.idx = 0; outer.idx < _nextFixedUpdateProcessSlot; outer.idx++) {
            if (FixedUpdateProcesses[outer.idx] != null) {
                if (outer.idx != inner.idx) {
                    FixedUpdateProcesses[inner.idx] = FixedUpdateProcesses[outer.idx];
                    FixedUpdatePaused[inner.idx] = FixedUpdatePaused[outer.idx];
                    MoveTag(outer, inner);

                    if (_indexToHandle.ContainsKey(inner)) {
                        _handleToIndex.Remove(_indexToHandle[inner]);
                        _indexToHandle.Remove(inner);
                    }

                    _handleToIndex[_indexToHandle[outer]] = inner;
                    _indexToHandle.Add(inner, _indexToHandle[outer]);
                    _indexToHandle.Remove(outer);
                }
                inner.idx++;
            }
        }
        for (outer.idx = inner.idx; outer.idx < _nextFixedUpdateProcessSlot; outer.idx++) {
            FixedUpdateProcesses[outer.idx] = null;
            FixedUpdatePaused[outer.idx] = false;
            RemoveTag(outer);

            if (_indexToHandle.ContainsKey(outer)) {
                _handleToIndex.Remove(_indexToHandle[outer]);
                _indexToHandle.Remove(outer);
            }
        }

        FixedUpdateCoroutines = _nextFixedUpdateProcessSlot = inner.idx;

        outer.seg = inner.seg = Segment.LateUpdate;
        for (outer.idx = inner.idx = 0; outer.idx < _nextLateUpdateProcessSlot; outer.idx++) {
            if (LateUpdateProcesses[outer.idx] != null) {
                if (outer.idx != inner.idx) {
                    LateUpdateProcesses[inner.idx] = LateUpdateProcesses[outer.idx];
                    LateUpdatePaused[inner.idx] = LateUpdatePaused[outer.idx];
                    MoveTag(outer, inner);

                    if (_indexToHandle.ContainsKey(inner)) {
                        _handleToIndex.Remove(_indexToHandle[inner]);
                        _indexToHandle.Remove(inner);
                    }

                    _handleToIndex[_indexToHandle[outer]] = inner;
                    _indexToHandle.Add(inner, _indexToHandle[outer]);
                    _indexToHandle.Remove(outer);
                }
                inner.idx++;
            }
        }
        for (outer.idx = inner.idx; outer.idx < _nextLateUpdateProcessSlot; outer.idx++) {
            LateUpdateProcesses[outer.idx] = null;
            LateUpdatePaused[outer.idx] = false;
            RemoveTag(outer);

            if (_indexToHandle.ContainsKey(outer)) {
                _handleToIndex.Remove(_indexToHandle[outer]);
                _indexToHandle.Remove(outer);
            }
        }

        LateUpdateCoroutines = _nextLateUpdateProcessSlot = inner.idx;

        outer.seg = inner.seg = Segment.SlowUpdate;
        for (outer.idx = inner.idx = 0; outer.idx < _nextSlowUpdateProcessSlot; outer.idx++) {
            if (SlowUpdateProcesses[outer.idx] != null) {
                if (outer.idx != inner.idx) {
                    SlowUpdateProcesses[inner.idx] = SlowUpdateProcesses[outer.idx];
                    SlowUpdatePaused[inner.idx] = SlowUpdatePaused[outer.idx];
                    MoveTag(outer, inner);

                    if (_indexToHandle.ContainsKey(inner)) {
                        _handleToIndex.Remove(_indexToHandle[inner]);
                        _indexToHandle.Remove(inner);
                    }

                    _handleToIndex[_indexToHandle[outer]] = inner;
                    _indexToHandle.Add(inner, _indexToHandle[outer]);
                    _indexToHandle.Remove(outer);
                }
                inner.idx++;
            }
        }
        for (outer.idx = inner.idx; outer.idx < _nextSlowUpdateProcessSlot; outer.idx++) {
            SlowUpdateProcesses[outer.idx] = null;
            SlowUpdatePaused[outer.idx] = false;
            RemoveTag(outer);

            if (_indexToHandle.ContainsKey(outer)) {
                _handleToIndex.Remove(_indexToHandle[outer]);
                _indexToHandle.Remove(outer);
            }
        }

        SlowUpdateCoroutines = _nextSlowUpdateProcessSlot = inner.idx;
    }

    /// <summary>
    /// Run a new coroutine in the Update segment.
    /// </summary>
    /// <param name="coroutine">The new coroutine's handle.</param>
    /// <returns>The coroutine's handle, which can be used for Wait and Kill operations.</returns>
    public static CoroutineHandle RunCoroutine(IEnumerator<float> coroutine) {
        return coroutine == null ? CoroutineHandle.Empty
            : Instance.RunCoroutineInternal(coroutine, Segment.Update, null, new CoroutineHandle(Instance._instanceID), true);
    }

    /// <summary>
    /// Run a new coroutine in the Update segment.
    /// </summary>
    /// <param name="coroutine">The new coroutine's handle.</param>
    /// <param name="tag">An optional tag to attach to the coroutine which can later be used for Kill operations.</param>
    /// <returns>The coroutine's handle, which can be used for Wait and Kill operations.</returns>
    public static CoroutineHandle RunCoroutine(IEnumerator<float> coroutine, string tag) {
        return coroutine == null ? CoroutineHandle.Empty
            : Instance.RunCoroutineInternal(coroutine, Segment.Update, tag, new CoroutineHandle(Instance._instanceID), true);
    }

    /// <summary>
    /// Run a new coroutine.
    /// </summary>
    /// <param name="coroutine">The new coroutine's handle.</param>
    /// <param name="timing">The segment that the coroutine should run in.</param>
    /// <returns>The coroutine's handle, which can be used for Wait and Kill operations.</returns>
    public static CoroutineHandle RunCoroutine(IEnumerator<float> coroutine, Segment timing) {
        return coroutine == null ? CoroutineHandle.Empty
            : Instance.RunCoroutineInternal(coroutine, timing, null, new CoroutineHandle(Instance._instanceID), true);
    }

    /// <summary>
    /// Run a new coroutine.
    /// </summary>
    /// <param name="coroutine">The new coroutine's handle.</param>
    /// <param name="timing">The segment that the coroutine should run in.</param>
    /// <param name="tag">An optional tag to attach to the coroutine which can later be used for Kill operations.</param>
    /// <returns>The coroutine's handle, which can be used for Wait and Kill operations.</returns>
    public static CoroutineHandle RunCoroutine(IEnumerator<float> coroutine, Segment timing, string tag) {
        return coroutine == null ? CoroutineHandle.Empty
            : Instance.RunCoroutineInternal(coroutine, timing, tag, new CoroutineHandle(Instance._instanceID), true);
    }

    /// <summary>
    /// Run a new coroutine on this Timing instance in the Update segment.
    /// </summary>
    /// <param name="coroutine">The new coroutine's handle.</param>
    /// <returns>The coroutine's handle, which can be used for Wait and Kill operations.</returns>
    public CoroutineHandle RunCoroutineOnInstance(IEnumerator<float> coroutine) {
        return coroutine == null ? CoroutineHandle.Empty
             : RunCoroutineInternal(coroutine, Segment.Update, null, new CoroutineHandle(_instanceID), true);
    }

    /// <summary>
    /// Run a new coroutine on this Timing instance in the Update segment.
    /// </summary>
    /// <param name="coroutine">The new coroutine's handle.</param>
    /// <param name="tag">An optional tag to attach to the coroutine which can later be used for Kill operations.</param>
    /// <returns>The coroutine's handle, which can be used for Wait and Kill operations.</returns>
    public CoroutineHandle RunCoroutineOnInstance(IEnumerator<float> coroutine, string tag) {
        return coroutine == null ? CoroutineHandle.Empty
             : RunCoroutineInternal(coroutine, Segment.Update, tag, new CoroutineHandle(_instanceID), true);
    }

    /// <summary>
    /// Run a new coroutine on this Timing instance.
    /// </summary>
    /// <param name="coroutine">The new coroutine's handle.</param>
    /// <param name="timing">The segment that the coroutine should run in.</param>
    /// <returns>The coroutine's handle, which can be used for Wait and Kill operations.</returns>
    public CoroutineHandle RunCoroutineOnInstance(IEnumerator<float> coroutine, Segment timing) {
        return coroutine == null ? CoroutineHandle.Empty
             : RunCoroutineInternal(coroutine, timing, null, new CoroutineHandle(_instanceID), true);
    }

    /// <summary>
    /// Run a new coroutine on this Timing instance.
    /// </summary>
    /// <param name="coroutine">The new coroutine's handle.</param>
    /// <param name="timing">The segment that the coroutine should run in.</param>
    /// <param name="tag">An optional tag to attach to the coroutine which can later be used for Kill operations.</param>
    /// <returns>The coroutine's handle, which can be used for Wait and Kill operations.</returns>
    public CoroutineHandle RunCoroutineOnInstance(IEnumerator<float> coroutine, Segment timing, string tag) {
        return coroutine == null ? CoroutineHandle.Empty
             : RunCoroutineInternal(coroutine, timing, tag, new CoroutineHandle(_instanceID), true);
    }

    private CoroutineHandle RunCoroutineInternal(IEnumerator<float> coroutine, Segment timing, string tag, CoroutineHandle handle, bool prewarm) {
        ProcessIndex slot = new ProcessIndex { seg = timing };

        if (_handleToIndex.ContainsKey(handle)) {
            _indexToHandle.Remove(_handleToIndex[handle]);
            _handleToIndex.Remove(handle);
        }

        switch (timing) {
            case Segment.Update:

                if (_nextUpdateProcessSlot >= UpdateProcesses.Length) {
                    IEnumerator<float>[] oldProcArray = UpdateProcesses;
                    bool[] oldPausedArray = UpdatePaused;

                    UpdateProcesses = new IEnumerator<float>[UpdateProcesses.Length + (ProcessArrayChunkSize * _expansions++)];
                    UpdatePaused = new bool[UpdateProcesses.Length];

                    for (int i = 0; i < oldProcArray.Length; i++) {
                        UpdateProcesses[i] = oldProcArray[i];
                        UpdatePaused[i] = oldPausedArray[i];
                    }
                }

                slot.idx = _nextUpdateProcessSlot++;
                UpdateProcesses[slot.idx] = coroutine;

                if (null != tag)
                    AddTag(tag, slot);

                _indexToHandle.Add(slot, handle);
                _handleToIndex.Add(handle, slot);

                if (!_runningUpdate && prewarm) {
                    try {
                        _runningUpdate = true;
                        UpdateTimeValues(slot.seg);

                        if (!UpdateProcesses[slot.idx].MoveNext()) {
                            UpdateProcesses[slot.idx] = null;
                        } else if (UpdateProcesses[slot.idx] != null && float.IsNaN(UpdateProcesses[slot.idx].Current)) {
                            if (ReplacementFunction == null) {
                                UpdateProcesses[slot.idx] = null;
                            } else {
                                UpdateProcesses[slot.idx] = ReplacementFunction(UpdateProcesses[slot.idx], _indexToHandle[slot]);

                                ReplacementFunction = null;
                            }
                        }
                    } catch (System.Exception ex) {
                        if (OnError == null)
                            _exceptions.Enqueue(ex);
                        else
                            OnError(ex);

                        UpdateProcesses[slot.idx] = null;
                    } finally {
                        _runningUpdate = false;
                    }
                }

                return handle;

            case Segment.FixedUpdate:

                if (_nextFixedUpdateProcessSlot >= FixedUpdateProcesses.Length) {
                    IEnumerator<float>[] oldProcArray = FixedUpdateProcesses;
                    bool[] oldPausedArray = FixedUpdatePaused;

                    FixedUpdateProcesses = new IEnumerator<float>[FixedUpdateProcesses.Length + (ProcessArrayChunkSize * _expansions++)];
                    FixedUpdatePaused = new bool[FixedUpdateProcesses.Length];

                    for (int i = 0; i < oldProcArray.Length; i++) {
                        FixedUpdateProcesses[i] = oldProcArray[i];
                        FixedUpdatePaused[i] = oldPausedArray[i];
                    }
                }

                slot.idx = _nextFixedUpdateProcessSlot++;
                FixedUpdateProcesses[slot.idx] = coroutine;

                if (null != tag)
                    AddTag(tag, slot);

                _indexToHandle.Add(slot, handle);
                _handleToIndex.Add(handle, slot);

                if (!_runningFixedUpdate && prewarm) {
                    try {
                        _runningFixedUpdate = true;
                        UpdateTimeValues(slot.seg);

                        if (!FixedUpdateProcesses[slot.idx].MoveNext()) {
                            FixedUpdateProcesses[slot.idx] = null;
                        } else if (FixedUpdateProcesses[slot.idx] != null && float.IsNaN(FixedUpdateProcesses[slot.idx].Current)) {
                            if (ReplacementFunction == null) {
                                FixedUpdateProcesses[slot.idx] = null;
                            } else {
                                FixedUpdateProcesses[slot.idx] = ReplacementFunction(FixedUpdateProcesses[slot.idx], _indexToHandle[slot]);

                                ReplacementFunction = null;
                            }
                        }
                    } catch (System.Exception ex) {
                        if (OnError == null)
                            _exceptions.Enqueue(ex);
                        else
                            OnError(ex);

                        FixedUpdateProcesses[slot.idx] = null;
                    } finally {
                        _runningFixedUpdate = false;
                    }
                }

                return handle;

            case Segment.LateUpdate:

                if (_nextLateUpdateProcessSlot >= LateUpdateProcesses.Length) {
                    IEnumerator<float>[] oldProcArray = LateUpdateProcesses;
                    bool[] oldPausedArray = LateUpdatePaused;

                    LateUpdateProcesses = new IEnumerator<float>[LateUpdateProcesses.Length + (ProcessArrayChunkSize * _expansions++)];
                    LateUpdatePaused = new bool[LateUpdateProcesses.Length];

                    for (int i = 0; i < oldProcArray.Length; i++) {
                        LateUpdateProcesses[i] = oldProcArray[i];
                        LateUpdatePaused[i] = oldPausedArray[i];
                    }
                }

                slot.idx = _nextLateUpdateProcessSlot++;
                LateUpdateProcesses[slot.idx] = coroutine;

                if (tag != null)
                    AddTag(tag, slot);

                _indexToHandle.Add(slot, handle);
                _handleToIndex.Add(handle, slot);

                if (!_runningLateUpdate && prewarm) {
                    try {
                        _runningLateUpdate = true;
                        UpdateTimeValues(slot.seg);

                        if (!LateUpdateProcesses[slot.idx].MoveNext()) {
                            LateUpdateProcesses[slot.idx] = null;
                        } else if (LateUpdateProcesses[slot.idx] != null && float.IsNaN(LateUpdateProcesses[slot.idx].Current)) {
                            if (ReplacementFunction == null) {
                                LateUpdateProcesses[slot.idx] = null;
                            } else {
                                LateUpdateProcesses[slot.idx] = ReplacementFunction(LateUpdateProcesses[slot.idx], _indexToHandle[slot]);

                                ReplacementFunction = null;
                            }
                        }
                    } catch (System.Exception ex) {
                        if (OnError == null)
                            _exceptions.Enqueue(ex);
                        else
                            OnError(ex);

                        LateUpdateProcesses[slot.idx] = null;
                    } finally {
                        _runningLateUpdate = false;
                    }
                }

                return handle;

            case Segment.SlowUpdate:

                if (_nextSlowUpdateProcessSlot >= SlowUpdateProcesses.Length) {
                    IEnumerator<float>[] oldProcArray = SlowUpdateProcesses;
                    bool[] oldPausedArray = SlowUpdatePaused;

                    SlowUpdateProcesses = new IEnumerator<float>[SlowUpdateProcesses.Length + (ProcessArrayChunkSize * _expansions++)];
                    SlowUpdatePaused = new bool[SlowUpdateProcesses.Length];

                    for (int i = 0; i < oldProcArray.Length; i++) {
                        SlowUpdateProcesses[i] = oldProcArray[i];
                        SlowUpdatePaused[i] = oldPausedArray[i];
                    }
                }

                slot.idx = _nextSlowUpdateProcessSlot++;
                SlowUpdateProcesses[slot.idx] = coroutine;

                if (tag != null)
                    AddTag(tag, slot);

                _indexToHandle.Add(slot, handle);
                _handleToIndex.Add(handle, slot);

                if (!_runningSlowUpdate && prewarm) {
                    try {
                        _runningSlowUpdate = true;
                        UpdateTimeValues(slot.seg);

                        if (!SlowUpdateProcesses[slot.idx].MoveNext()) {
                            SlowUpdateProcesses[slot.idx] = null;
                        } else if (SlowUpdateProcesses[slot.idx] != null && float.IsNaN(SlowUpdateProcesses[slot.idx].Current)) {
                            if (ReplacementFunction == null) {
                                SlowUpdateProcesses[slot.idx] = null;
                            } else {
                                SlowUpdateProcesses[slot.idx] = ReplacementFunction(SlowUpdateProcesses[slot.idx], _indexToHandle[slot]);

                                ReplacementFunction = null;
                            }
                        }
                    } catch (System.Exception ex) {
                        if (OnError == null)
                            _exceptions.Enqueue(ex);
                        else
                            OnError(ex);

                        SlowUpdateProcesses[slot.idx] = null;
                    } finally {
                        _runningSlowUpdate = false;
                    }
                }

                return handle;

            default:
                return CoroutineHandle.Empty;
        }
    }

    /// <summary>
    /// This will kill all coroutines running on the main MEC instance and reset the context.
    /// </summary>
    /// <returns>The number of coroutines that were killed.</returns>
    public static int KillCoroutines() {
        return _instance == null ? 0 : _instance.KillCoroutinesOnInstance();
    }

    /// <summary>
    /// This will kill all coroutines running on the current MEC instance and reset the context.
    /// </summary>
    /// <returns>The number of coroutines that were killed.</returns>
    public int KillCoroutinesOnInstance() {
        int retVal = _nextUpdateProcessSlot + _nextLateUpdateProcessSlot + _nextFixedUpdateProcessSlot + _nextSlowUpdateProcessSlot;

        UpdateProcesses = new IEnumerator<float>[InitialBufferSizeLarge];
        UpdatePaused = new bool[InitialBufferSizeLarge];
        UpdateCoroutines = 0;
        _nextUpdateProcessSlot = 0;

        LateUpdateProcesses = new IEnumerator<float>[InitialBufferSizeSmall];
        LateUpdatePaused = new bool[InitialBufferSizeSmall];
        LateUpdateCoroutines = 0;
        _nextLateUpdateProcessSlot = 0;

        FixedUpdateProcesses = new IEnumerator<float>[InitialBufferSizeMedium];
        FixedUpdatePaused = new bool[InitialBufferSizeMedium];
        FixedUpdateCoroutines = 0;
        _nextFixedUpdateProcessSlot = 0;

        SlowUpdateProcesses = new IEnumerator<float>[InitialBufferSizeMedium];
        SlowUpdatePaused = new bool[InitialBufferSizeMedium];
        SlowUpdateCoroutines = 0;
        _nextSlowUpdateProcessSlot = 0;

        _processTags.Clear();
        _taggedProcesses.Clear();
        _handleToIndex.Clear();
        _indexToHandle.Clear();
        _waitingTriggers.Clear();
        _expansions = (ushort)((_expansions / 2) + 1);

        ResetTimeCountOnInstance();

        return retVal;
    }

    /// <summary>
    /// Kills the instances of the coroutine handle if it exists.
    /// </summary>
    /// <param name="handle">The handle of the coroutine to kill.</param>
    /// <returns>The number of coroutines that were found and killed (0 or 1).</returns>
    public static int KillCoroutines(CoroutineHandle handle) {
        return ActiveInstances.ContainsKey(handle.Key) ? GetInstance(handle.Key).KillCoroutinesOnInstance(handle) : 0;
    }

    /// <summary>
    /// Kills the instance of the coroutine handle on this Timing instance if it exists.
    /// </summary>
    /// <param name="handle">The handle of the coroutine to kill.</param>
    /// <returns>The number of coroutines that were found and killed (0 or 1).</returns>
    public int KillCoroutinesOnInstance(CoroutineHandle handle) {
        bool foundOne = false;

        if (_handleToIndex.ContainsKey(handle)) {
            if (_waitingTriggers.ContainsKey(handle))
                CloseWaitingProcess(handle);

            foundOne = CoindexExtract(_handleToIndex[handle]) != null;
            RemoveTag(_handleToIndex[handle]);
        }

        return foundOne ? 1 : 0;
    }

    /// <summary>
    /// Kills all coroutines that have the given tag.
    /// </summary>
    /// <param name="tag">All coroutines with this tag will be killed.</param>
    /// <returns>The number of coroutines that were found and killed.</returns>
    public static int KillCoroutines(string tag) {
        return _instance == null ? 0 : _instance.KillCoroutinesOnInstance(tag);
    }

    /// <summary> 
    /// Kills all coroutines that have the given tag.
    /// </summary>
    /// <param name="tag">All coroutines with this tag will be killed.</param>
    /// <returns>The number of coroutines that were found and killed.</returns>
    public int KillCoroutinesOnInstance(string tag) {
        if (tag == null) return 0;
        int numberFound = 0;

        while (_taggedProcesses.ContainsKey(tag)) {
            var matchEnum = _taggedProcesses[tag].GetEnumerator();
            matchEnum.MoveNext();

            if (CoindexKill(matchEnum.Current)) {
                if (_waitingTriggers.ContainsKey(_indexToHandle[matchEnum.Current]))
                    CloseWaitingProcess(_indexToHandle[matchEnum.Current]);

                numberFound++;
            }

            RemoveTag(matchEnum.Current);

            if (_indexToHandle.ContainsKey(matchEnum.Current)) {
                _handleToIndex.Remove(_indexToHandle[matchEnum.Current]);
                _indexToHandle.Remove(matchEnum.Current);
            }
        }

        return numberFound;
    }

    /// <summary>
    /// This will pause all coroutines running on the current MEC instance until ResumeCoroutines is called.
    /// </summary>
    /// <returns>The number of coroutines that were paused.</returns>
    public static int PauseCoroutines() {
        return _instance == null ? 0 : _instance.PauseCoroutinesOnInstance();
    }

    /// <summary>
    /// This will pause all coroutines running on this MEC instance until ResumeCoroutinesOnInstance is called.
    /// </summary>
    /// <returns>The number of coroutines that were paused.</returns>
    public int PauseCoroutinesOnInstance() {
        int count = 0;
        int i;
        for (i = 0; i < _nextUpdateProcessSlot; i++) {
            if (!UpdatePaused[i] && UpdateProcesses[i] != null) {
                UpdatePaused[i] = true;
                count++;
            }
        }

        for (i = 0; i < _nextLateUpdateProcessSlot; i++) {
            if (!LateUpdatePaused[i] && LateUpdateProcesses[i] != null) {
                LateUpdatePaused[i] = true;
                count++;
            }
        }

        for (i = 0; i < _nextFixedUpdateProcessSlot; i++) {
            if (!FixedUpdatePaused[i] && FixedUpdateProcesses[i] != null) {
                FixedUpdatePaused[i] = true;
                count++;
            }
        }

        for (i = 0; i < _nextSlowUpdateProcessSlot; i++) {
            if (!SlowUpdatePaused[i] && SlowUpdateProcesses[i] != null) {
                SlowUpdatePaused[i] = true;
                count++;
            }
        }

        return count;
    }

    /// <summary>
    /// This will pause any matching coroutines running on the current MEC instance until ResumeCoroutines is called.
    /// </summary>
    /// <param name="tag">Any coroutines with a matching tag will be paused.</param>
    /// <returns>The number of coroutines that were paused.</returns>
    public static int PauseCoroutines(string tag) {
        return _instance == null ? 0 : _instance.PauseCoroutinesOnInstance(tag);
    }

    /// <summary>
    /// This will pause any matching coroutines running on this MEC instance until ResumeCoroutinesOnInstance is called.
    /// </summary>
    /// <param name="tag">Any coroutines with a matching tag will be paused.</param>
    /// <returns>The number of coroutines that were paused.</returns>
    public int PauseCoroutinesOnInstance(string tag) {
        if (tag == null || !_taggedProcesses.ContainsKey(tag))
            return 0;

        int count = 0;
        var matchesEnum = _taggedProcesses[tag].GetEnumerator();

        while (matchesEnum.MoveNext())
            if (!CoindexIsNull(matchesEnum.Current) && !CoindexSetPause(matchesEnum.Current))
                count++;

        return count;
    }

    /// <summary>
    /// This resumes all coroutines on the current MEC instance if they are currently paused, otherwise it has
    /// no effect.
    /// </summary>
    /// <returns>The number of coroutines that were resumed.</returns>
    public static int ResumeCoroutines() {
        return _instance == null ? 0 : _instance.ResumeCoroutinesOnInstance();
    }

    /// <summary>
    /// This resumes all coroutines on this MEC instance if they are currently paused, otherwise it has no effect.
    /// </summary>
    /// <returns>The number of coroutines that were resumed.</returns>
    public int ResumeCoroutinesOnInstance() {
        int count = 0;
        int i;
        for (i = 0; i < _nextUpdateProcessSlot; i++) {
            if (UpdatePaused[i] && UpdateProcesses[i] != null) {
                UpdatePaused[i] = false;
                count++;
            }
        }

        for (i = 0; i < _nextLateUpdateProcessSlot; i++) {
            if (LateUpdatePaused[i] && LateUpdateProcesses[i] != null) {
                LateUpdatePaused[i] = false;
                count++;
            }
        }

        for (i = 0; i < _nextFixedUpdateProcessSlot; i++) {
            if (FixedUpdatePaused[i] && FixedUpdateProcesses[i] != null) {
                FixedUpdatePaused[i] = false;
                count++;
            }
        }

        for (i = 0; i < _nextSlowUpdateProcessSlot; i++) {
            if (SlowUpdatePaused[i] && SlowUpdateProcesses[i] != null) {
                SlowUpdatePaused[i] = false;
                count++;
            }
        }

        var waitingEnum = _waitingTriggers.GetEnumerator();
        while (waitingEnum.MoveNext()) {
            int listCount = 0;
            var pausedList = waitingEnum.Current.Value.GetEnumerator();

            while (pausedList.MoveNext()) {
                if (_handleToIndex.ContainsKey(pausedList.Current.Handle) && !CoindexIsNull(_handleToIndex[pausedList.Current.Handle])) {
                    CoindexSetPause(_handleToIndex[pausedList.Current.Handle]);
                    listCount++;
                } else {
                    waitingEnum.Current.Value.Remove(pausedList.Current);
                    listCount = 0;
                    pausedList = waitingEnum.Current.Value.GetEnumerator();
                }
            }

            count -= listCount;
        }

        return count;
    }

    /// <summary>
    /// This resumes any matching coroutines on the current MEC instance if they are currently paused, otherwise it has
    /// no effect.
    /// </summary>
    /// <param name="tag">Any coroutines previously paused with a matching tag will be resumend.</param>
    /// <returns>The number of coroutines that were resumed.</returns>
    public static int ResumeCoroutines(string tag) {
        return _instance == null ? 0 : _instance.ResumeCoroutinesOnInstance(tag);
    }

    /// <summary>
    /// This resumes any matching coroutines on this MEC instance if they are currently paused, otherwise it has no effect.
    /// </summary>
    /// <param name="tag">Any coroutines previously paused with a matching tag will be resumend.</param>
    /// <returns>The number of coroutines that were resumed.</returns>
    public int ResumeCoroutinesOnInstance(string tag) {
        if (tag == null || !_taggedProcesses.ContainsKey(tag))
            return 0;
        int count = 0;

        var indexesEnum = _taggedProcesses[tag].GetEnumerator();
        while (indexesEnum.MoveNext())
            if (!CoindexIsNull(indexesEnum.Current) && CoindexSetPause(indexesEnum.Current, false))
                count++;

        var waitingEnum = _waitingTriggers.GetEnumerator();
        while (waitingEnum.MoveNext()) {
            var pausedList = waitingEnum.Current.Value.GetEnumerator();
            while (pausedList.MoveNext()) {
                if (_handleToIndex.ContainsKey(pausedList.Current.Handle) && !CoindexIsNull(_handleToIndex[pausedList.Current.Handle])
                    && !CoindexSetPause(_handleToIndex[pausedList.Current.Handle]))
                    count--;
            }
        }

        return count;
    }

    private void UpdateTimeValues(Segment segment) {
        switch (segment) {
            case Segment.Update:
            case Segment.LateUpdate:
                if (_currentUpdateFrame != Time.frameCount) {
                    deltaTime = Time.deltaTime;
                    _lastUpdateTime += deltaTime;
                    localTime = _lastUpdateTime;
                    _currentUpdateFrame = Time.frameCount;
                } else {
                    deltaTime = Time.deltaTime;
                    localTime = _lastUpdateTime;
                }
                return;
            case Segment.FixedUpdate:
                if (_currentFixedUpdateFrame != Time.frameCount) {
                    deltaTime = Time.deltaTime;
                    _lastFixedUpdateTime += deltaTime;
                    localTime = _lastFixedUpdateTime;
                    _currentFixedUpdateFrame = Time.frameCount;
                } else {
                    deltaTime = Time.deltaTime;
                    localTime = _lastFixedUpdateTime;
                }
                return;
            case Segment.SlowUpdate:
                if (_currentSlowUpdateFrame != Time.frameCount) {
                    deltaTime = _lastSlowUpdateDeltaTime = Time.realtimeSinceStartup - (float)_lastSlowUpdateTime;
                    localTime = _lastSlowUpdateTime = Time.realtimeSinceStartup;
                    _currentSlowUpdateFrame = Time.frameCount;
                } else {
                    deltaTime = _lastSlowUpdateDeltaTime;
                    localTime = _lastSlowUpdateTime;
                }
                return;
        }
    }

    private double GetSegmentTime(Segment segment) {
        switch (segment) {
            case Segment.Update:
            case Segment.LateUpdate:
                if (_currentUpdateFrame == Time.frameCount)
                    return _lastUpdateTime;
                else
                    return _lastUpdateTime + Time.deltaTime;
            case Segment.FixedUpdate:
                if (_currentFixedUpdateFrame == Time.frameCount)
                    return _lastFixedUpdateTime;
                else
                    return _lastFixedUpdateTime + Time.deltaTime;
            case Segment.SlowUpdate:
                return Time.realtimeSinceStartup;
            default:
                return 0d;
        }
    }

    /// <summary>
    /// Not all segments can have their local time value reset to zero, but the ones that can are reset through this function.
    /// </summary>
    public void ResetTimeCountOnInstance() {
        localTime = 0d;

        _lastUpdateTime = 0d;
        _lastFixedUpdateTime = 0d;
    }

    /// <summary>
    /// Retrieves the MEC manager that corresponds to the supplied instance id.
    /// </summary>
    /// <param name="ID">The instance ID.</param>
    /// <returns>The manager, or null if not found.</returns>
    public static EzTiming GetInstance(byte ID) {
        return ActiveInstances.ContainsKey(ID) ? ActiveInstances[ID] : null;
    }

    private void AddTag(string tag, ProcessIndex coindex) {
        _processTags.Add(coindex, tag);

        if (_taggedProcesses.ContainsKey(tag))
            _taggedProcesses[tag].Add(coindex);
        else
            _taggedProcesses.Add(tag, new HashSet<ProcessIndex> { coindex });
    }

    private void RemoveTag(ProcessIndex coindex) {
        if (_processTags.ContainsKey(coindex)) {
            if (_taggedProcesses[_processTags[coindex]].Count > 1)
                _taggedProcesses[_processTags[coindex]].Remove(coindex);
            else
                _taggedProcesses.Remove(_processTags[coindex]);

            _processTags.Remove(coindex);
        }
    }

    private void MoveTag(ProcessIndex coindexFrom, ProcessIndex coindexTo) {
        RemoveTag(coindexTo);

        if (_processTags.ContainsKey(coindexFrom)) {
            _taggedProcesses[_processTags[coindexFrom]].Remove(coindexFrom);
            _taggedProcesses[_processTags[coindexFrom]].Add(coindexTo);

            _processTags.Add(coindexTo, _processTags[coindexFrom]);
            _processTags.Remove(coindexFrom);
        }
    }

    private bool CoindexKill(ProcessIndex coindex) {
        bool result = false;
        switch (coindex.seg) {
            case Segment.Update:
                result = UpdateProcesses[coindex.idx] != null;
                UpdateProcesses[coindex.idx] = null;
                break;
            case Segment.FixedUpdate:
                result = FixedUpdateProcesses[coindex.idx] != null;
                FixedUpdateProcesses[coindex.idx] = null;
                break;
            case Segment.LateUpdate:
                result = LateUpdateProcesses[coindex.idx] != null;
                LateUpdateProcesses[coindex.idx] = null;
                break;
            case Segment.SlowUpdate:
                result = SlowUpdateProcesses[coindex.idx] != null;
                SlowUpdateProcesses[coindex.idx] = null;
                break;
        }
        return result;
    }

    private IEnumerator<float> CoindexExtract(ProcessIndex coindex) {
        IEnumerator<float> result = null;
        switch (coindex.seg) {
            case Segment.Update:
                result = UpdateProcesses[coindex.idx];
                UpdateProcesses[coindex.idx] = null;
                break;
            case Segment.FixedUpdate:
                result = FixedUpdateProcesses[coindex.idx];
                FixedUpdateProcesses[coindex.idx] = null;
                break;
            case Segment.LateUpdate:
                result = LateUpdateProcesses[coindex.idx];
                LateUpdateProcesses[coindex.idx] = null;
                break;
            case Segment.SlowUpdate:
                result = SlowUpdateProcesses[coindex.idx];
                SlowUpdateProcesses[coindex.idx] = null;
                break;
        }
        return result;
    }

    private IEnumerator<float> CoindexPeek(ProcessIndex coindex) {
        switch (coindex.seg) {
            case Segment.Update:
                return UpdateProcesses[coindex.idx];
            case Segment.FixedUpdate:
                return FixedUpdateProcesses[coindex.idx];
            case Segment.LateUpdate:
                return LateUpdateProcesses[coindex.idx];
            case Segment.SlowUpdate:
                return SlowUpdateProcesses[coindex.idx];
        }
        return null;
    }

    private bool CoindexIsNull(ProcessIndex coindex) {
        switch (coindex.seg) {
            case Segment.Update:
                return UpdateProcesses[coindex.idx] == null;
            case Segment.FixedUpdate:
                return FixedUpdateProcesses[coindex.idx] == null;
            case Segment.LateUpdate:
                return LateUpdateProcesses[coindex.idx] == null;
            case Segment.SlowUpdate:
                return SlowUpdateProcesses[coindex.idx] == null;
        }
        return true;
    }

    private bool CoindexSetPause(ProcessIndex coindex, bool newPausedState = true) {
        bool paused = false;
        switch (coindex.seg) {
            case Segment.Update:
                paused = UpdatePaused[coindex.idx];
                UpdatePaused[coindex.idx] = newPausedState;
                break;
            case Segment.FixedUpdate:
                paused = FixedUpdatePaused[coindex.idx];
                FixedUpdatePaused[coindex.idx] = newPausedState;
                break;
            case Segment.LateUpdate:
                paused = LateUpdatePaused[coindex.idx];
                LateUpdatePaused[coindex.idx] = newPausedState;
                break;
            case Segment.SlowUpdate:
                paused = SlowUpdatePaused[coindex.idx];
                SlowUpdatePaused[coindex.idx] = newPausedState;
                break;
        }
        return paused;
    }

    private void CoindexReplace(ProcessIndex coindex, IEnumerator<float> replacement) {
        switch (coindex.seg) {
            case Segment.Update:
                UpdateProcesses[coindex.idx] = replacement;
                break;
            case Segment.FixedUpdate:
                FixedUpdateProcesses[coindex.idx] = replacement;
                break;
            case Segment.LateUpdate:
                LateUpdateProcesses[coindex.idx] = replacement;
                break;
            case Segment.SlowUpdate:
                SlowUpdateProcesses[coindex.idx] = replacement;
                break;
        }
    }

    private static IEnumerator<float> _InjectDelay(IEnumerator<float> proc, double returnAt) {
        yield return (float)returnAt;

        ReplacementFunction = delegate { return proc; };
        yield return float.NaN;
    }

    /// <summary>
    /// Use in a yield return statement to wait for the specified number of seconds.
    /// </summary>
    /// <param name="waitTime">Number of seconds to wait.</param>
    public static float WaitForSeconds(float waitTime) {
        if (float.IsNaN(waitTime)) waitTime = 0f;
        return LocalTime + waitTime;
    }

    /// <summary>
    /// Use in a yield return statement to wait for the specified number of seconds.
    /// </summary>
    /// <param name="waitTime">Number of seconds to wait.</param>
    public float WaitForSecondsOnInstance(float waitTime) {
        if (float.IsNaN(waitTime)) waitTime = 0f;
        return (float)localTime + waitTime;
    }

    /// <summary>
    /// Use the command "yield return Timing.WaitUntilDone(otherCoroutine);" to pause the current 
    /// coroutine until otherCoroutine is done.
    /// </summary>
    /// <param name="otherCoroutine">The coroutine to pause for.</param>
    public static float WaitUntilDone(CoroutineHandle otherCoroutine) {
        return WaitUntilDone(otherCoroutine, true);
    }

    /// <summary>
    /// Use the command "yield return Timing.WaitUntilDone(otherCoroutine);" to pause the current 
    /// coroutine until otherCoroutine is done.
    /// </summary>
    /// <param name="otherCoroutine">The coroutine to pause for.</param>
    /// <param name="warnOnIssue">Post a warning to the console if no hold action was actually performed.</param>
    public static float WaitUntilDone(CoroutineHandle otherCoroutine, bool warnOnIssue) {
        EzTiming inst = GetInstance(otherCoroutine.Key);

        if (inst != null && inst._handleToIndex.ContainsKey(otherCoroutine)) {
            if (inst.CoindexIsNull(inst._handleToIndex[otherCoroutine]))
                return 0f;

            if (!inst._waitingTriggers.ContainsKey(otherCoroutine)) {
                inst.CoindexReplace(inst._handleToIndex[otherCoroutine],
                    inst._StartWhenDone(otherCoroutine, inst.CoindexPeek(inst._handleToIndex[otherCoroutine])));
                inst._waitingTriggers.Add(otherCoroutine, new HashSet<ProcessData>());
            }

            ReplacementFunction = (coptr, handle) => {
                if (handle == otherCoroutine) {
                    if (warnOnIssue)
                        Debug.LogWarning("A coroutine attempted to wait for itself.");

                    return coptr;
                }
                if (handle.Key != otherCoroutine.Key) {
                    if (warnOnIssue)
                        Debug.LogWarning("A coroutine attempted to wait for a coroutine running on a different MEC instance.");

                    return coptr;
                }

                inst._waitingTriggers[otherCoroutine].Add(new ProcessData {
                    Handle = handle,
                    PauseTime = coptr.Current > inst.GetSegmentTime(inst._handleToIndex[handle].seg)
                        ? coptr.Current - (float)inst.GetSegmentTime(inst._handleToIndex[handle].seg) : 0f
                });

                inst.CoindexSetPause(inst._handleToIndex[handle]);

                return coptr;
            };

            return float.NaN;
        }

        if (warnOnIssue)
            Debug.LogWarning("WaitUntilDone cannot hold: The coroutine handle that was passed in is invalid.\n" + otherCoroutine);

        return 0f;
    }

    private IEnumerator<float> _StartWhenDone(CoroutineHandle handle, IEnumerator<float> proc) {
        if (!_waitingTriggers.ContainsKey(handle)) yield break;

        try {
            if (proc.Current > localTime)
                yield return proc.Current;

            while (proc.MoveNext())
                yield return proc.Current;
        } finally {
            CloseWaitingProcess(handle);
        }
    }

    private void CloseWaitingProcess(CoroutineHandle handle) {
        if (!_waitingTriggers.ContainsKey(handle)) return;

        var tasksEnum = _waitingTriggers[handle].GetEnumerator();
        _waitingTriggers.Remove(handle);

        while (tasksEnum.MoveNext()) {
            if (_handleToIndex.ContainsKey(tasksEnum.Current.Handle)) {
                ProcessIndex coIndex = _handleToIndex[tasksEnum.Current.Handle];

                if (tasksEnum.Current.PauseTime > 0d)
                    CoindexReplace(coIndex, _InjectDelay(CoindexPeek(coIndex), (float)(GetSegmentTime(coIndex.seg) + tasksEnum.Current.PauseTime)));

                CoindexSetPause(coIndex, false);
            }
        }
    }

    /// <summary>
    /// Use the command "yield return Timing.WaitUntilDone(wwwObject);" to pause the current 
    /// coroutine until the wwwObject is done.
    /// </summary>
    /// <param name="wwwObject">The www object to pause for.</param>
    public static float WaitUntilDone(WWW wwwObject) {
        if (wwwObject == null || wwwObject.isDone) return 0f;
        ReplacementFunction = (input, tag) => _StartWhenDone(wwwObject, input);
        return float.NaN;
    }

    private static IEnumerator<float> _StartWhenDone(WWW www, IEnumerator<float> pausedProc) {
        while (!www.isDone)
            yield return 0f;

        ReplacementFunction = delegate { return pausedProc; };
        yield return float.NaN;
    }

    /// <summary>
    /// Use the command "yield return Timing.WaitUntilDone(operation);" to pause the current 
    /// coroutine until the operation is done.
    /// </summary>
    /// <param name="operation">The operation variable returned.</param>
    public static float WaitUntilDone(AsyncOperation operation) {
        if (operation == null || operation.isDone) return 0f;
        ReplacementFunction = (input, tag) => _StartWhenDone(operation, input);
        return float.NaN;
    }

    private static IEnumerator<float> _StartWhenDone(AsyncOperation operation, IEnumerator<float> pausedProc) {
        while (!operation.isDone)
            yield return 0f;

        ReplacementFunction = delegate { return pausedProc; };
        yield return float.NaN;
    }

#if !UNITY_4_6 && !UNITY_4_7 && !UNITY_5_0 && !UNITY_5_1 && !UNITY_5_2
    /// <summary>
    /// Use the command "yield return Timing.WaitUntilDone(operation);" to pause the current 
    /// coroutine until the operation is done.
    /// </summary>
    /// <param name="operation">The operation variable returned.</param>
    public static float WaitUntilDone(CustomYieldInstruction operation) {
        if (operation == null || !operation.keepWaiting) return 0f;
        ReplacementFunction = (input, tag) => _StartWhenDone(operation, input);
        return float.NaN;
    }

    private static IEnumerator<float> _StartWhenDone(CustomYieldInstruction operation, IEnumerator<float> pausedProc) {
        while (operation.keepWaiting)
            yield return 0f;

        ReplacementFunction = delegate { return pausedProc; };
        yield return float.NaN;
    }
#endif

    /// <summary>
    /// Keeps this coroutine from executing until UnlockCoroutine is called with a matching key.
    /// </summary>
    /// <param name="coroutine">The handle to the coroutine to be locked.</param>
    /// <param name="key">The key to use. A new key can be generated by calling "new CoroutineHandle(0)".</param>
    /// <returns>Whether the lock was successful.</returns>
    public bool LockCoroutine(CoroutineHandle coroutine, CoroutineHandle key) {
        if (coroutine.Key != _instanceID || key == CoroutineHandle.Empty || key.Key != 0) {
            return false;
        }
        if (!_waitingTriggers.ContainsKey(key))
            _waitingTriggers.Add(key, new HashSet<ProcessData>());

        _waitingTriggers[key].Add(new ProcessData { Handle = coroutine });
        CoindexSetPause(_handleToIndex[coroutine]);

        return true;
    }

    /// <summary>
    /// Unlocks a coroutine that has been locked, so long as the key matches.
    /// </summary>
    /// <param name="coroutine">The handle to the coroutine to be unlocked.</param>
    /// <param name="key">The key that the coroutine was previously locked with.</param>
    /// <returns>Whether the coroutine was successfully unlocked.</returns>
    public bool UnlockCoroutine(CoroutineHandle coroutine, CoroutineHandle key) {
        if (coroutine.Key != _instanceID || key == CoroutineHandle.Empty ||
            !_handleToIndex.ContainsKey(coroutine) || !_waitingTriggers.ContainsKey(key)) {
            return false;
        }
        ProcessData wrappedCoroutine = new ProcessData { Handle = coroutine };
        _waitingTriggers[key].Remove(wrappedCoroutine);

        bool coroutineStillPaused = false;
        var triggersEnum = _waitingTriggers.GetEnumerator();
        while (triggersEnum.MoveNext()) {
            if (triggersEnum.Current.Value.Contains(wrappedCoroutine))
                coroutineStillPaused = true;
        }
        CoindexSetPause(_handleToIndex[coroutine], coroutineStillPaused);
        return true;
    }

    #region CallDelayed

    /// <summary>
    /// Calls the specified action after a specified number of seconds.
    /// </summary>
    /// <param name="delay">The number of seconds to wait before calling the action.</param>
    /// <param name="action">The action to call.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallDelayed(float delay, System.Action action, 
        Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallDelayed(delay, action), timing);
    }

    /// <summary>
    /// Calls the specified action after a specified number of seconds.
    /// </summary>
    /// <param name="delay">The number of seconds to wait before calling the action.</param>
    /// <param name="action">The action to call.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallDelayedOnInstance(float delay, System.Action action,
        Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallDelayed(delay, action), timing);
    }

    private IEnumerator<float> _CallDelayed(float delay, System.Action action) {
        yield return WaitForSecondsOnInstance(delay);
        action();
    }

    /// <summary>
    /// Calls the specified action after a specified number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="delay">The number of seconds to wait before calling the action.</param>
    /// <param name="action">The action to call.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallDelayed<T>(T reference, float delay, System.Action<T> action,
        Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallDelayed(reference, delay, action), timing, tag);
    }

    /// <summary>
    /// Calls the specified action after a specified number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="delay">The number of seconds to wait before calling the action.</param>
    /// <param name="action">The action to call.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallDelayedOnInstance<T>(T reference, float delay, System.Action<T> action,
        Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallDelayed(reference, delay, action), timing, tag);
    }

    private IEnumerator<float> _CallDelayed<T>(T reference, float delay, System.Action<T> action) {
        yield return WaitForSecondsOnInstance(delay);
        action(reference);
    }

    #endregion

    #region CallPeriodically

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="duration">The number of seconds that this function should run.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallPeriodically(float duration, float interval,
        System.Action action, System.Action onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallPeriodically(duration, interval, action, onDone), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="duration">The number of seconds that this function should run.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallPeriodicallyOnInstance(float duration, float interval,
        System.Action action, System.Action onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallPeriodically(duration, interval, action, onDone), timing, tag);
    }

    private IEnumerator<float> _CallPeriodically(float duration, float interval,
        System.Action action, System.Action onDone) {
        double startTime = localTime;
        while (localTime <= startTime + duration) {
            action();
            yield return WaitForSecondsOnInstance(interval);
        }
        if (onDone != null)
            onDone();
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="duration">The number of seconds that this function should run.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallPeriodically<T>(T reference, float duration, float interval,
        System.Action<T> action, System.Action<T> onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallPeriodically(reference, duration, interval, action, onDone), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="duration">The number of seconds that this function should run.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallPeriodicallyOnInstance<T>(T reference, float duration, float interval,
        System.Action<T> action, System.Action<T> onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallPeriodically(reference, duration, interval, action, onDone), timing, tag);
    }

    private IEnumerator<float> _CallPeriodically<T>(T reference, float duration, float interval,
        System.Action<T> action, System.Action<T> onDone = null) {
        double startTime = localTime;
        while (localTime <= startTime + duration) {
            action(reference);
            yield return WaitForSecondsOnInstance(interval);
        }
        if (onDone != null)
            onDone(reference);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="duration">The number of seconds that this function should run.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallPeriodically(float duration, float[] intervals,
        System.Action action, System.Action onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallPeriodically(duration, intervals, action, onDone), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="duration">The number of seconds that this function should run.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallPeriodicallyOnInstance(float duration, float[] intervals,
        System.Action action, System.Action onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallPeriodically(duration, intervals, action, onDone), timing, tag);
    }

    private IEnumerator<float> _CallPeriodically(float duration, float[] intervals,
        System.Action action, System.Action onDone) {
        double startTime = localTime;
        int count = 0;
        while (localTime <= startTime + duration) {
            action();
            yield return WaitForSecondsOnInstance(intervals[count++ % intervals.Length]);
        }
        if (onDone != null)
            onDone();
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="duration">The number of seconds that this function should run.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallPeriodically<T>(T reference, float duration, float[] intervals,
        System.Action<T> action, System.Action<T> onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallPeriodically(reference, duration, intervals, action, onDone), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="duration">The number of seconds that this function should run.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallPeriodicallyOnInstance<T>(T reference, float duration, float[] intervals,
        System.Action<T> action, System.Action<T> onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallPeriodically(reference, duration, intervals, action, onDone), timing, tag);
    }

    private IEnumerator<float> _CallPeriodically<T>(T reference, float duration, float[] intervals,
        System.Action<T> action, System.Action<T> onDone = null) {
        double startTime = localTime;
        int count = 0;
        while (localTime <= startTime + duration) {
            action(reference);
            yield return WaitForSecondsOnInstance(intervals[count++ % intervals.Length]);
        }
        if (onDone != null)
            onDone(reference);
    }

    #endregion

    #region CallAtTimes

    /// <summary>
    /// Calls the supplied action at the given times and rate.
    /// </summary>
    /// <param name="times">The times that this function should run.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallAtTimes(int times, float interval,
        System.Action action, System.Action onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallAtTimes(times, interval, action, onDone), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given times and rate.
    /// </summary>
    /// <param name="times">The times that this function should run.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallAtTimesOnInstance(int times, float interval,
        System.Action action, System.Action onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallAtTimes(times, interval, action, onDone), timing, tag);
    }

    private IEnumerator<float> _CallAtTimes(int times, float interval,
        System.Action action, System.Action onDone) {
        int count = 0;
        while (count++ < times) {
            action();
            yield return WaitForSecondsOnInstance(interval);
        }
        if (onDone != null)
            onDone();
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="times">The times that this function should run.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallAtTimes<T>(T reference, int times, float interval,
        System.Action<T> action, System.Action<T> onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallAtTimes(reference, times, interval, action, onDone), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="times">The number of seconds that this function should run.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallAtTimesOnInstance<T>(T reference, int times, float interval,
        System.Action<T> action, System.Action<T> onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallAtTimes(reference, times, interval, action, onDone), timing, tag);
    }

    private IEnumerator<float> _CallAtTimes<T>(T reference, int times, float interval,
        System.Action<T> action, System.Action<T> onDone = null) {
        int count = 0;
        while (count++ < times) {
            action(reference);
            yield return WaitForSecondsOnInstance(interval);
        }
        if (onDone != null)
            onDone(reference);
    }

    /// <summary>
    /// Calls the supplied action at the given times and rate.
    /// </summary>
    /// <param name="times">The times that this function should run.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallAtTimes(int times, float[] intervals,
        System.Action action, System.Action onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallAtTimes(times, intervals, action, onDone), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given times and rate.
    /// </summary>
    /// <param name="times">The times that this function should run.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallAtTimesOnInstance(int times, float[] intervals,
        System.Action action, System.Action onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallAtTimes(times, intervals, action, onDone), timing, tag);
    }

    private IEnumerator<float> _CallAtTimes(int times, float[] intervals,
        System.Action action, System.Action onDone) {
        for (int count = 0; count < times; ++count) {
            action();
            yield return WaitForSecondsOnInstance(intervals[count % intervals.Length]);
        }
        if (onDone != null)
            onDone();
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="times">The times that this function should run.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallAtTimes<T>(T reference, int times, float[] intervals,
        System.Action<T> action, System.Action<T> onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallAtTimes(reference, times, intervals, action, onDone), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="times">The number of seconds that this function should run.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="onDone">An optional action to call when this function finishes.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallAtTimesOnInstance<T>(T reference, int times, float[] intervals,
        System.Action<T> action, System.Action<T> onDone = null, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallAtTimes(reference, times, intervals, action, onDone), timing, tag);
    }

    private IEnumerator<float> _CallAtTimes<T>(T reference, int times, float[] intervals,
        System.Action<T> action, System.Action<T> onDone = null) {
        for (int count = 0; count < times; ++count) {
            action(reference);
            yield return WaitForSecondsOnInstance(intervals[count % intervals.Length]);
        }
        if (onDone != null)
            onDone(reference);
    }

    #endregion

    #region CallForever

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallForever(float interval, 
        System.Action action, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallForever(interval, action), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallForeverOnInstance(float interval, 
        System.Action action, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallForever(interval, action), timing, tag);
    }

    private IEnumerator<float> _CallForever(float intervall, System.Action action) {
        while (true) {
            action();
            yield return WaitForSecondsOnInstance(intervall);
        }
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallForever<T>(T reference, float interval, 
        System.Action<T> action, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallForever(reference, interval, action), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallForeverOnInstance<T>(T reference, float interval, 
        System.Action<T> action, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallForever(reference, interval, action), timing, tag);
    }

    private IEnumerator<float> _CallForever<T>(T reference, float interval, System.Action<T> action) {
        while (true) {
            action(reference);
            yield return WaitForSecondsOnInstance(interval);
        }
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallForever(float[] intervals,
        System.Action action, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallForever(intervals, action), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallForeverOnInstance(float[] intervals,
        System.Action action, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallForever(intervals, action), timing, tag);
    }

    private IEnumerator<float> _CallForever(float[] intervals, System.Action action) {
        int count = 0;
        while (true) {
            action();
            yield return WaitForSecondsOnInstance(intervals[count++ % intervals.Length]);
        }
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallForever<T>(T reference, float[] intervals,
        System.Action<T> action, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallForever(reference, intervals, action), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="action">The action to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallForeverOnInstance<T>(T reference, float[] intervals,
        System.Action<T> action, Segment timing = Segment.Update, string tag = null) {
        return action == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallForever(reference, intervals, action), timing, tag);
    }

    private IEnumerator<float> _CallForever<T>(T reference, float[] intervals, System.Action<T> action) {
        int count = 0;
        while (true) {
            action(reference);
            yield return WaitForSecondsOnInstance(intervals[count++ % intervals.Length]);
        }
    }

    #endregion

    #region CallUntil

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="func">The function to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallUntil(float interval, 
        System.Func<bool> func, Segment timing = Segment.Update, string tag = null) {
        return func == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallUntil(interval, func), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="func">The function to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallUntilOnInstance(float interval, 
        System.Func<bool> func, Segment timing = Segment.Update, string tag = null) {
        return func == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallUntil(interval, func), timing, tag);
    }

    private IEnumerator<float> _CallUntil(float interval, System.Func<bool> func) {
        bool loop = true;
        while (loop) {
            loop = func();
            yield return WaitForSecondsOnInstance(interval);
        }
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="func">The function to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallUntil<T>(T reference, float interval, 
        System.Func<T, bool> func, Segment timing = Segment.Update, string tag = null) {
        return func == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallUntil(reference, interval, func), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="interval">The amount of time between calls.</param>
    /// <param name="func">The function to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallUntilOnInstance<T>(T reference, float interval, 
        System.Func<T, bool> func, Segment timing = Segment.Update, string tag = null) {
        return func == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallUntil(reference, interval, func), timing, tag);
    }

    private IEnumerator<float> _CallUntil<T>(T reference, float interval, System.Func<T, bool> func) {
        bool loop = true;
        while (loop) {
            loop = func(reference);
            yield return WaitForSecondsOnInstance(interval);
        }
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="func">The function to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallUntil(float[] intervals,
        System.Func<bool> func, Segment timing = Segment.Update, string tag = null) {
        return func == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallUntil(intervals, func), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="func">The function to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallUntilOnInstance(float[] intervals,
        System.Func<bool> func, Segment timing = Segment.Update, string tag = null) {
        return func == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallUntil(intervals, func), timing, tag);
    }

    private IEnumerator<float> _CallUntil(float[] intervals, System.Func<bool> func) {
        bool loop = true;
        int count = 0;
        while (loop) {
            loop = func();
            yield return WaitForSecondsOnInstance(intervals[count++ % intervals.Length]);
        }
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="func">The function to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public static CoroutineHandle CallUntil<T>(T reference, float[] intervals,
        System.Func<T, bool> func, Segment timing = Segment.Update, string tag = null) {
        return func == null ? CoroutineHandle.Empty :
            RunCoroutine(Instance._CallUntil(reference, intervals, func), timing, tag);
    }

    /// <summary>
    /// Calls the supplied action at the given rate for a given number of seconds.
    /// </summary>
    /// <param name="reference">A value that will be passed in to the supplied action each period.</param>
    /// <param name="intervals">The amount of times between calls.</param>
    /// <param name="func">The function to call every frame.</param>
    /// <param name="timing">The timing segment to run in.</param>
    /// <param name="tag">The tag of the coroutine.</param>
    /// <returns>The handle to the coroutine that is started by this function.</returns>
    public CoroutineHandle CallUntilOnInstance<T>(T reference, float[] intervals,
        System.Func<T, bool> func, Segment timing = Segment.Update, string tag = null) {
        return func == null ? CoroutineHandle.Empty :
            RunCoroutineOnInstance(_CallUntil(reference, intervals, func), timing, tag);
    }

    private IEnumerator<float> _CallUntil<T>(T reference, float[] intervals, System.Func<T, bool> func) {
        bool loop = true;
        int count = 0;
        while (loop) {
            loop = func(reference);
            yield return WaitForSecondsOnInstance(intervals[count++ % intervals.Length]);
        }
    }

    #endregion

    private struct ProcessData : System.IEquatable<ProcessData> {
        public CoroutineHandle Handle;
        public float PauseTime;

        public bool Equals(ProcessData other) {
            return Handle == other.Handle;
        }

        public override bool Equals(object other) {
            if (other is ProcessData)
                return Equals((ProcessData)other);
            return false;
        }

        public override int GetHashCode() {
            return Handle.GetHashCode();
        }
    }

    private struct ProcessIndex : System.IEquatable<ProcessIndex> {
        public Segment seg;
        public int idx;

        public bool Equals(ProcessIndex other) {
            return seg == other.seg && idx == other.idx;
        }

        public override bool Equals(object other) {
            if (other is ProcessIndex)
                return Equals((ProcessIndex)other);
            return false;
        }

        public static bool operator ==(ProcessIndex a, ProcessIndex b) {
            return a.seg == b.seg && a.idx == b.idx;
        }

        public static bool operator !=(ProcessIndex a, ProcessIndex b) {
            return a.seg != b.seg || a.idx != b.idx;
        }

        public override int GetHashCode() {
            return (((int)seg - 2) * (int.MaxValue / 3)) + idx;
        }
    }

    [System.Obsolete("Unity coroutine function, use RunCoroutine instead.", true)]
    public new Coroutine StartCoroutine(System.Collections.IEnumerator routine) { return null; }

    [System.Obsolete("Unity coroutine function, use RunCoroutine instead.", true)]
    public new Coroutine StartCoroutine(string methodName, object value) { return null; }

    [System.Obsolete("Unity coroutine function, use RunCoroutine instead.", true)]
    public new Coroutine StartCoroutine(string methodName) { return null; }

    [System.Obsolete("Unity coroutine function, use RunCoroutine instead.", true)]
    public new Coroutine StartCoroutine_Auto(System.Collections.IEnumerator routine) { return null; }

    [System.Obsolete("Unity coroutine function, use KillCoroutine instead.", true)]
    public new void StopCoroutine(string methodName) { }

    [System.Obsolete("Unity coroutine function, use KillCoroutine instead.", true)]
    public new void StopCoroutine(System.Collections.IEnumerator routine) { }

    [System.Obsolete("Unity coroutine function, use KillCoroutine instead.", true)]
    public new void StopCoroutine(Coroutine routine) { }

    [System.Obsolete("Unity coroutine function, use KillAllCoroutines instead.", true)]
    public new void StopAllCoroutines() { }

    [System.Obsolete("Use your own GameObject for this.", true)]
    public new static void Destroy(Object obj) { }

    [System.Obsolete("Use your own GameObject for this.", true)]
    public new static void Destroy(Object obj, float f) { }

    [System.Obsolete("Use your own GameObject for this.", true)]
    public new static void DestroyObject(Object obj) { }

    [System.Obsolete("Use your own GameObject for this.", true)]
    public new static void DestroyObject(Object obj, float f) { }

    [System.Obsolete("Use your own GameObject for this.", true)]
    public new static void DestroyImmediate(Object obj) { }

    [System.Obsolete("Use your own GameObject for this.", true)]
    public new static void DestroyImmediate(Object obj, bool b) { }

    [System.Obsolete("Just.. no.", true)]
    public new static T FindObjectOfType<T>() where T : Object { return null; }

    [System.Obsolete("Just.. no.", true)]
    public new static Object FindObjectOfType(System.Type t) { return null; }

    [System.Obsolete("Just.. no.", true)]
    public new static T[] FindObjectsOfType<T>() where T : Object { return null; }

    [System.Obsolete("Just.. no.", true)]
    public new static Object[] FindObjectsOfType(System.Type t) { return null; }

    [System.Obsolete("Just.. no.", true)]
    public new static void print(object message) { }
}

public enum Segment {
    Invalid = -1,
    Update,
    FixedUpdate,
    LateUpdate,
    SlowUpdate,
}

/// <summary>
/// A handle for a MEC coroutine.
/// </summary>
public struct CoroutineHandle : System.IEquatable<CoroutineHandle> {
    private const byte ReservedSpace = 0x1F;
    private readonly static int[] NextIndex = {
        ReservedSpace + 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0
    };
    private readonly int _id;

    public byte Key { get { return (byte)(_id & ReservedSpace); } }

    public static readonly CoroutineHandle Empty = new CoroutineHandle();

    public CoroutineHandle(byte ind) {
        if (ind > ReservedSpace)
            ind -= ReservedSpace;

        _id = NextIndex[ind] + ind;
        NextIndex[ind] += ReservedSpace + 1;
    }

    public bool Equals(CoroutineHandle other) {
        return _id == other._id;
    }

    public override bool Equals(object other) {
        if (other is CoroutineHandle)
            return Equals((CoroutineHandle)other);
        return false;
    }

    public static bool operator ==(CoroutineHandle a, CoroutineHandle b) {
        return a._id == b._id;
    }

    public static bool operator !=(CoroutineHandle a, CoroutineHandle b) {
        return a._id != b._id;
    }

    public override int GetHashCode() {
        return _id;
    }

    /// <summary>
    /// Is true if this handle may have been a valid handle at some point. (i.e. is not an uninitialized handle, error handle, or a key to a coroutine lock)
    /// </summary>
    public bool IsValid {
        get { return Key != 0; }
    }
}


public static class EzTimuingExtensionMethods {
    /// <summary>
    /// Cancels this coroutine when the supplied game object is destroyed or made inactive.
    /// </summary>
    /// <param name="coroutine">The coroutine handle to act upon.</param>
    /// <param name="gameObject">The GameObject to test.</param>
    /// <returns>The modified coroutine handle.</returns>
    public static IEnumerator<float> CancelWith(this IEnumerator<float> coroutine, 
        GameObject gameObject) {
        while (EzTiming.MainThread != System.Threading.Thread.CurrentThread || 
            (gameObject && gameObject.activeInHierarchy && coroutine.MoveNext()))
            yield return coroutine.Current;
    }

    /// <summary>
    /// Cancels this coroutine when the supplied game objects are destroyed or made inactive.
    /// </summary>
    /// <param name="coroutine">The coroutine handle to act upon.</param>
    /// <param name="gameObject1">The first GameObject to test.</param>
    /// <param name="gameObject2">The second GameObject to test</param>
    /// <returns>The modified coroutine handle.</returns>
    public static IEnumerator<float> CancelWith(this IEnumerator<float> coroutine, 
        GameObject gameObject1, GameObject gameObject2) {
        while (EzTiming.MainThread != System.Threading.Thread.CurrentThread || 
            (gameObject1 != null && gameObject1.activeInHierarchy &&
             gameObject2 != null && gameObject2.activeInHierarchy && 
             coroutine.MoveNext()))
            yield return coroutine.Current;
    }

    /// <summary>
    /// Cancels this coroutine when the supplied game objects are destroyed or made inactive.
    /// </summary>
    /// <param name="coroutine">The coroutine handle to act upon.</param>
    /// <param name="gameObject1">The first GameObject to test.</param>
    /// <param name="gameObject2">The second GameObject to test</param>
    /// <param name="gameObject3">The third GameObject to test.</param>
    /// <returns>The modified coroutine handle.</returns>
    public static IEnumerator<float> CancelWith(this IEnumerator<float> coroutine,
        GameObject gameObject1, GameObject gameObject2, GameObject gameObject3) {
        while (EzTiming.MainThread != System.Threading.Thread.CurrentThread || 
            (gameObject1 != null && gameObject1.activeInHierarchy &&
             gameObject2 != null && gameObject2.activeInHierarchy &&
             gameObject3 != null && gameObject3.activeInHierarchy && 
             coroutine.MoveNext()))
            yield return coroutine.Current;
    }
}
