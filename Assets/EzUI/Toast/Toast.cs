﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Toast : MonoBehaviour {

    public float showTime = 0.5f;
    public float ascendHeight = 30f;
    public float duration = 0.5f;

    private Image bg;
    private Text text;
    private Vector3 localPosition;

    public static void Show(string message) {
        Toast toast = ToastManager.GetInstance().Create();
        toast._Show(message);
    }

    public static void Show(string message, float duration) {
        Toast toast = ToastManager.GetInstance().Create();
        toast._Show(message, duration);
    }

    void Awake() {
        bg = GetComponent<Image>();
        text = GetComponentInChildren<Text>();
        localPosition = transform.localPosition;
    }

    void _Show(string message) {
        text.text = message;
        iTween.MoveBy(gameObject, iTween.Hash("y", ascendHeight, "time", showTime,
            "easetype", iTween.EaseType.easeInOutSine));
        iTween.ValueTo(gameObject, iTween.Hash("from", 0f, "to", 1f, "time", showTime, 
            "onupdate", "UpdateAlpha", "oncomplete", "Hide"));
    }

    void _Show(string message, float duration) {
        this.duration = duration;
        Show(message);
    }

    void Hide() {
        iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "time", showTime, "delay", duration,
            "onupdate", "UpdateAlpha", "oncomplete", "Recycle"));
    }

    void UpdateAlpha(float alpha) {
        bg.color = new Color(bg.color.r, bg.color.g, bg.color.b, alpha / 4);
        text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
    }

    void Recycle() {
        transform.localPosition = localPosition;
        gameObject.SetActive(false);
        ToastManager.GetInstance().Recycle(this);
    }

}
