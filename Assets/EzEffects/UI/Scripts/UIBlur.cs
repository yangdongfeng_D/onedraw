﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBlur : MonoBehaviour {

    public float size = 1;

    private Image image;

    void OnEnable() {
        image = GetComponent<Image>();
        UpdateBlur();
    }

    void OnDisable() {
        UpdateBlur();
    }

    void Update() {
        UpdateBlur();
    }

    void UpdateBlur() {
        image.material.SetFloat("Size", Mathf.Clamp(size, 0, 20));
    }
}
