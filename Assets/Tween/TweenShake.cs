﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TweenShake : Tween {

    public Vector3 amount;
    [Serializable]
    public enum ShakeType {
        Position,
        Rotation,
        Scale,
    }

    public ShakeType shakeType = ShakeType.Position;
    public bool localSpace = true;
    public bool resetOnComplete = true;

    private Vector3 origin;

    public override void PlayNow() {
        switch (shakeType) {
            case ShakeType.Position:
                origin = localSpace ? transform.localPosition : transform.position;
                iTween.ShakePosition(gameObject, iTween.Hash("amount", amount,
                    "time", time, "delay", delay, "islocal", localSpace, 
                    "looptype", loopType, "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this));
                break;
            case ShakeType.Rotation:
                origin = localSpace ? transform.localEulerAngles : transform.eulerAngles;
                iTween.ShakeRotation(gameObject, iTween.Hash("amount", amount,
                    "time", time, "delay", delay, "space", localSpace ? Space.Self : Space.World, 
                    "looptype", loopType, "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this));
                break;
            case ShakeType.Scale:
                origin = transform.localScale;
                iTween.ShakeScale(gameObject, iTween.Hash("amount", amount,
                    "time", time, "delay", delay, "space",
                    "looptype", loopType, "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this));
                break;
        }
    }

    public void Reset() {
        switch (shakeType) {
            case ShakeType.Position:
                if (localSpace) {
                    transform.localPosition = origin;
                } else {
                    transform.position = origin;
                }
                break;
            case ShakeType.Rotation:
                if (localSpace) {
                    transform.localEulerAngles = origin;
                } else {
                    transform.eulerAngles = origin;
                }
                break;
            case ShakeType.Scale:
                transform.localScale = origin;
                break;
        }
    }

    public override string Type() {
        return "shake";
    }

    protected override void OnComplete(Tween tween) {
        if (tween != this) return;
        if (resetOnComplete) {
            Reset();
        }
        base.OnComplete(tween);
    }
}
