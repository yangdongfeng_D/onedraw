﻿using UnityEngine;

public class IntTweenable : ITweenable<int> {
    public virtual int Diff(int start, int end) { return end - start; }
    public virtual int Lerp(int start, int diff, float time) { return Mathf.RoundToInt(start + diff * time); }
}

public class FloatTweenable : ITweenable<float> {
    public virtual float Diff(float start, float end) { return end - start; }
    public virtual float Lerp(float start, float diff, float time) { return start + diff * time; }
}

public class Vector2Tweenable : ITweenable<Vector2> {
    public virtual Vector2 Diff(Vector2 start, Vector2 end) { return end - start; }
    public virtual Vector2 Lerp(Vector2 start, Vector2 diff, float time) { return start + diff * time; }
}

public class Vector3Tweenable : ITweenable<Vector3> {
    public virtual Vector3 Diff(Vector3 start, Vector3 end) { return end - start; }
    public virtual Vector3 Lerp(Vector3 start, Vector3 diff, float time) { return start + diff * time; }
}

public class ColorTweenable : ITweenable<Color> {
    public virtual Color Diff(Color start, Color end) { return end - start; }
    public virtual Color Lerp(Color start, Color diff, float time) { return start + diff * time; }
}
