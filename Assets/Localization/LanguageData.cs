﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class LanguageData : ISerializationCallbackReceiver {

    public string type;

    public Dictionary<string, string> data;

    public List<string> dataKeys;
    public List<string> dataValues;

    public override string ToString() {
        string result = "Type:" + type;
        List<string> keys = new List<string>(data.Keys);
        for (int i = 0; i < keys.Count; ++i) {
            result += "\nKey:[" + keys[i] + "] ==> Value:[" + data[keys[i]] + "]";
        }
        return result;
    }

    public void OnBeforeSerialize() {
        dataKeys = new List<string>(data.Keys);
        dataValues = new List<string>(data.Values);
    }

    public void OnAfterDeserialize() {
        int count = Math.Min(dataKeys.Count, dataValues.Count);
        data = new Dictionary<string, string>(count);
        for (var i = 0; i < count; ++i) {
            data.Add(dataKeys[i], dataValues[i]);
        }
    }
}
