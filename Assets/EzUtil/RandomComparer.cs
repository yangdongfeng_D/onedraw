﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomComparer<T> : IComparer<T> {

    private static readonly int[] vs = { -1, 0, 1 };

    public int Compare(T x, T y) {
        return vs[Random.Range(0, vs.Length)];
    }
}
