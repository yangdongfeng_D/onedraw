﻿using UnityEngine;
using System.Collections;
using System.Globalization;

public static class StringExtension {

    public static Color ToColor(this string hexString) {
        if (hexString.StartsWith("#")) {
            hexString = hexString.Substring(1);
        }
        if (hexString.StartsWith("0x")) {
            hexString = hexString.Substring(2);
        }
        if (hexString.Length != 6) {
            return Color.white;
        }
        byte r = byte.Parse(hexString.Substring(0, 2), NumberStyles.HexNumber);
        byte g = byte.Parse(hexString.Substring(2, 2), NumberStyles.HexNumber);
        byte b = byte.Parse(hexString.Substring(4, 2), NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }
	
}
