﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedFont : MonoBehaviour {

    // Use this for initialization
    void Start() {
        Font font = GameManager.instance.localFont;
        if (font != null) {
            GetComponent<Text>().font = font;
        }
    }
}
