﻿public static class EzEaseLinear {
    public static float None(float t, float b, float c, float d) {
        return c * t / d + b;
    }
}