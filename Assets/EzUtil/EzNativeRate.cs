﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;


public class EzNativeRate {

    public static void NativeRate() {
#if UNITY_IOS
			nativeRate();
#endif
    }

#if UNITY_IOS
	[DllImport("__Internal")]
	private static extern void nativeRate();
#endif
}
