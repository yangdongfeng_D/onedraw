﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LocalizedText : MonoBehaviour {

    public string key;

	// Use this for initialization
	void Start() {
        GetComponent<Text>().text = Localization.GetMultilineText(key);
        ContentSizeFitter fitter = GetComponent<ContentSizeFitter>();
        if (fitter != null) {
            fitter.SetLayoutHorizontal();
        }
    }

}
