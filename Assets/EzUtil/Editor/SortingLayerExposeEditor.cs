﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;
using System.Reflection;

[CustomEditor(typeof(SortingLayerExposed))]
public class SortingLayerExposedEditor : Editor {

    string[] sortingLayerNames;

    void OnEnable() {
        sortingLayerNames = GetSortingLayerNames();
    }

    public override void OnInspectorGUI() {
        // Get the renderer from the target object
        var renderer = (target as SortingLayerExposed).gameObject.GetComponent<Renderer>();
        if (!renderer) {
            return;
        }

        // Expose the sorting layer name
        int index = Mathf.Max(0, System.Array.IndexOf(sortingLayerNames, renderer.sortingLayerName));
        index = EditorGUILayout.Popup("Sorting Layer", index, sortingLayerNames);
        string sortingLayerName = sortingLayerNames[index];
        if (sortingLayerName != renderer.sortingLayerName) {
            Undo.RecordObject(renderer, "Edit Sorting Layer");
            renderer.sortingLayerName = sortingLayerName;
            EditorUtility.SetDirty(renderer);
        }

        // Expose the manual sorting order
        int sortingOrder = EditorGUILayout.IntField("Order In Layer", renderer.sortingOrder);
        if (sortingOrder != renderer.sortingOrder) {
            Undo.RecordObject(renderer, "Edit Sorting Order");
            renderer.sortingOrder = sortingOrder;
            EditorUtility.SetDirty(renderer);
        }
    }

    public string[] GetSortingLayerNames() {
        Type internalEditorUtilityType = typeof(InternalEditorUtility);
        PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames",
            BindingFlags.Static | BindingFlags.NonPublic);
        string[] sortingLayers = (string[])sortingLayersProperty.GetValue(null, new object[0]);
        return sortingLayers;
    }
}