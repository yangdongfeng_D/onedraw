﻿using UnityEngine;
using System.Collections;

public class TweenMove : Tween {

    public Vector3 position;
    public bool offsetMode = true;
    public bool localSpace = true;
    public bool useLossyScale = true;
    public Transform lookTarget;
    public float lookTime;

    public override void PlayNow() {
        if (offsetMode) {
            if (useLossyScale) position.Scale(transform.lossyScale);
            Hashtable args = iTween.Hash("amount", position,
                "time", time, "delay", delay, "space", localSpace ? Space.Self : Space.World,
                "easetype", easeType, "looptype", loopType,
                "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this);
            if (lookTarget != null) {
                args.Add("looktarget", lookTarget);
                if (lookTime > 0) args.Add("looktime", lookTime);
            }
            iTween.MoveBy(gameObject, args);
        } else {
            Hashtable args = iTween.Hash("position", position,
                "time", time, "delay", delay, "islocal", localSpace,
                "easetype", easeType, "looptype", loopType,
                "onupdate", "OnUpdate", "oncomplete", "OnComplete", "oncompleteparams", this);
            if (lookTarget != null) {
                args.Add("looktarget", lookTarget);
                if (lookTime > 0) args.Add("looktime", lookTime);
            }
            iTween.MoveTo(gameObject, args);
        }
    }

    public override string Type() {
        return "move";
    }
}
