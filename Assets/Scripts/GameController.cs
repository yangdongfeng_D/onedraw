﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : Singleton<GameController> {

    // Use this for initialization
    void Start() {
        GameManager.instance.OnGameStart();
    }

    // Update is called once per frame
    void Update() {
        Panel.CheckBackButton(() => GameManager.instance.LoadScene("Home"));
    }

    public void Revive() {
        // Todo: revive player
    }

    public void GameOver() {
        GameManager.instance.OnGameOver();
    }
}
