﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IntText : MonoBehaviour {

    public string format = "{0:D}";
    public string formatKey;

    public int targetValue;
    public float changeSpeed = 10;

    [System.Serializable]
    public class ChangeEvent : UnityEvent<int> { }
    public ChangeEvent onChange;

    private const float TOL = 1e-3f;

    private Text text;
    private float value;
    private float maxDiff;

	void Awake() {
        text = GetComponent<Text>();
        if (!string.IsNullOrEmpty(formatKey)) {
            format = Localization.GetMultilineText(formatKey);
            formatKey = "";
        }
    }
	
	// Update is called once per frame
	void Update() {
        float diff = Mathf.Abs(value - targetValue);
        maxDiff = Mathf.Max(maxDiff, diff);
        if (diff < maxDiff * TOL) {
            if (diff > 0) {
                value = targetValue;
                if (onChange != null) {
                    onChange.Invoke(targetValue);
                }
            }
        } else {
            value = Mathf.Lerp(value, targetValue, Time.deltaTime * changeSpeed);
        }
        text.text = string.Format(format, Mathf.RoundToInt(value));
	}

    public void SetValue(int value) {
        targetValue = value;
        this.value = value;
    }
}
