﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public string format = "{0:D}";
    public string formatKey;

    private IntText intText;
	private Text text;

	void Awake() {
        if (!string.IsNullOrEmpty(formatKey)) {
            format = Localization.GetMultilineText(formatKey);
            formatKey = "";
        }
        intText = GetComponent<IntText>();
        if (intText != null) {
            intText.format = format;
            intText.formatKey = formatKey;
        } else {
            text = GetComponent<Text>();
        }
		Update();
	}
	
	// Update is called once per frame
	void Update() {
        if (intText != null) {
            intText.targetValue = GameManager.instance.score;
        } else if (text != null) {
            text.text = string.Format(format, GameManager.instance.score);
        }
	}
}
