﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EzTweener : MonoBehaviour {

    public TargetObject target;

    [System.Serializable]
    public enum PropertyType {
        Unknown,
        Int,
        Float,
        Vector2,
        Vector3,
        Color
    }

    [System.Serializable]
    public class PropertyItem {
        public string name;
        public PropertyType type;
        public int intValue;
        public float floatValue;
        public Vector2 vector2Value;
        public Vector3 vector3Value;
        public Color colorValue;
        public bool relative;
    }
    public List<PropertyItem> propertyItems;

    public float duration = 1f;
    public float delay = 0f;
    public EzEaseType easeType;
    public EzLoopType loopType;
    public int loops = 1;
    public float timeScale = 1f;
    public bool fromMode = false;
    public bool playOnStart = true;
    public float playDelay = 0f;
    public bool playBackward = false;

    [System.Serializable] public class InitEvent : UnityEvent { }
    public InitEvent onInit; // executes before initial setup.

    [System.Serializable] public class StartEvent : UnityEvent { }
    public StartEvent onStart; // executes when a tween starts.

    [System.Serializable] public class UpdateEvent : UnityEvent { }
    public CompleteEvent onUpdate; // execute whenever a tween updates.

    [System.Serializable] public class CompleteEvent : UnityEvent { }
    public LoopEndEvent onComplete; // exectures whenever a tween completes

    [System.Serializable] public class LoopBeginEvent : UnityEvent { }
    public UpdateEvent onLoopBegin; // executes whenever a tween starts an iteration.

    [System.Serializable] public class LoopEndEvent : UnityEvent { }
    public LoopBeginEvent onLoopEnd; // executes whenever a tween ends an iteration.

    private EzTween tween;

    // Use this for initialization
    void Start() {
        if (playOnStart) {
            if (playDelay > 0) {
                EzTiming.CallDelayed(playDelay, Play);
            } else {
                Play();
            }
        }
    }

    public void Play() {
        if (duration <= 0) {
            Debug.LogWarning("Duration must be greater than 0.");
            duration = 1f;
        }
        EzTweenConfig config = new EzTweenConfig()
            .SetDelay(delay)
            .SetEaseType(easeType)
            .SetLoops(loops, loopType)
            .SetTimeScale(timeScale)
            .SetFromMode(fromMode);
        foreach (PropertyItem item in propertyItems) {
            switch (item.type) {
                case PropertyType.Int:
                    config.AddTweenProperty(new EzTweenProperty<int, IntTweenable>(item.name, item.intValue, item.relative));
                    break;
                case PropertyType.Float:
                    config.AddTweenProperty(new EzTweenProperty<float, FloatTweenable>(item.name, item.floatValue, item.relative));
                    break;
                case PropertyType.Vector2:
                    config.AddTweenProperty(new EzTweenProperty<Vector2, Vector2Tweenable>(item.name, item.vector2Value, item.relative));
                    break;
                case PropertyType.Vector3:
                    config.AddTweenProperty(new EzTweenProperty<Vector3, Vector3Tweenable>(item.name, item.vector3Value, item.relative));
                    break;
                case PropertyType.Color:
                    config.AddTweenProperty(new EzTweenProperty<Color, ColorTweenable>(item.name, item.colorValue, item.relative));
                    break;
            }
        }
        tween = new EzTween(target.target, duration, config);
        tween.OnInit((_) => { if (onInit != null) onInit.Invoke(); });
        tween.OnStart((_) => { if (onStart != null) onStart.Invoke(); });
        tween.OnUpdate((_) => { if (onUpdate != null) onUpdate.Invoke(); });
        tween.OnComplete((_) => { if (onComplete != null) onComplete.Invoke(); });
        tween.OnLoopBegin((_) => { if (onLoopBegin != null) onLoopBegin.Invoke(); });
        tween.OnLoopEnd((_) => { if (onLoopEnd != null) onLoopEnd.Invoke(); });
        EzTweenManager.AddTween(tween);
        if (playBackward) {
            PlayBackward();
        } else {
            PlayForward();
        }
    }

    public void PlayForward() {
        if (tween != null) {
            tween.PlayForward();
        }
    }

    public void PlayBackward() {
        if (tween != null) {
            tween.PlayBackward();
        }
    }

    public void Complete() {
        if (tween != null) {
            tween.Complete();
        }
    }

    public void Pause() {
        if (tween != null) {
            tween.Pause();
        }
    }

    public void Resume() {
        if (tween != null) {
            tween.Play();
        }
    }

    public void Reset() {
        if (tween != null) {
            tween.Rewind();
        }
    }
}
