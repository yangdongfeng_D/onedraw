﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using UnityEngine;
using System.IO;
using System;

public class EzCrypto {

    public static string defaultPassword { get { return EzIds.appIDLocalized; } }

    public static string MD5(string input) {
        var inputBytes = Encoding.UTF8.GetBytes(input);
        var md5 = new MD5CryptoServiceProvider();
        var hash = md5.ComputeHash(inputBytes);
        var stringBuilder = new StringBuilder();
        for (var i = 0; i < hash.Length; ++i) {
            stringBuilder.Append(hash[i].ToString("x2"));
        }
        return stringBuilder.ToString();
    }

    public static string SHA256(string input, string key) {
        var inputBytes = Encoding.UTF8.GetBytes(input);
        var keyBytes = Encoding.UTF8.GetBytes(input);
        HMACSHA256 sha256 = new HMACSHA256(keyBytes);
        var hash = sha256.ComputeHash(inputBytes);
        var stringBuilder = new StringBuilder();
        for (var i = 0; i < hash.Length; ++i) {
            stringBuilder.Append(hash[i].ToString("X2"));
        }
        return stringBuilder.ToString();
    }

    public static string SHA256(string input) {
        return SHA256(input, defaultPassword);
    }

    public static string EncryptString(string input, ICryptoTransform encryptor) {
        byte[] inputBytes = Encoding.UTF8.GetBytes(input);
        byte[] outputBytes = encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Convert.ToBase64String(outputBytes);
    }

    public static string DecryptString(string input, ICryptoTransform decryptor) {
        byte[] inputBytes = Convert.FromBase64String(input);
        byte[] outputBytes = decryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Encoding.UTF8.GetString(outputBytes);
    }

    public static string EncryptBool(bool input, ICryptoTransform encryptor) {
        byte[] inputBytes = BitConverter.GetBytes(input);
        byte[] outputBytes = encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Convert.ToBase64String(outputBytes);
    }

    public static bool DecryptBool(string input, ICryptoTransform decryptor) {
        byte[] inputBytes = Convert.FromBase64String(input);
        byte[] outputBytes = decryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return BitConverter.ToBoolean(outputBytes, 0);
    }

    public static string EncryptChar(char input, ICryptoTransform encryptor) {
        byte[] inputBytes = BitConverter.GetBytes(input);
        byte[] outputBytes = encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Convert.ToBase64String(outputBytes);
    }

    public static char DecryptChar(string input, ICryptoTransform decryptor) {
        byte[] inputBytes = Convert.FromBase64String(input);
        byte[] outputBytes = decryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return BitConverter.ToChar(outputBytes, 0);
    }

    public static string EncryptShort(short input, ICryptoTransform encryptor) {
        byte[] inputBytes = BitConverter.GetBytes(input);
        byte[] outputBytes = encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Convert.ToBase64String(outputBytes);
    }

    public static short DecryptShort(string input, ICryptoTransform decryptor) {
        byte[] inputBytes = Convert.FromBase64String(input);
        byte[] outputBytes = decryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return BitConverter.ToInt16(outputBytes, 0);
    }

    public static string EncryptInt(int input, ICryptoTransform encryptor) {
        byte[] inputBytes = BitConverter.GetBytes(input);
        byte[] outputBytes = encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Convert.ToBase64String(outputBytes);
    }

    public static int DecryptInt(string input, ICryptoTransform decryptor) {
        byte[] inputBytes = Convert.FromBase64String(input);
        byte[] outputBytes = decryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return BitConverter.ToInt32(outputBytes, 0);
    }

    public static string EncryptLong(long input, ICryptoTransform encryptor) {
        byte[] inputBytes = BitConverter.GetBytes(input);
        byte[] outputBytes = encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Convert.ToBase64String(outputBytes);
    }

    public static long DecryptLong(string input, ICryptoTransform decryptor) {
        byte[] inputBytes = Convert.FromBase64String(input);
        byte[] outputBytes = decryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return BitConverter.ToInt64(outputBytes, 0);
    }

    public static string EncryptFloat(float input, ICryptoTransform encryptor) {
        byte[] inputBytes = BitConverter.GetBytes(input);
        byte[] outputBytes = encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Convert.ToBase64String(outputBytes);
    }

    public static float DecryptFloat(string input, ICryptoTransform decryptor) {
        byte[] inputBytes = Convert.FromBase64String(input);
        byte[] outputBytes = decryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return BitConverter.ToSingle(outputBytes, 0);
    }

    public static string EncryptDouble(double input, ICryptoTransform encryptor) {
        byte[] inputBytes = BitConverter.GetBytes(input);
        byte[] outputBytes = encryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return Convert.ToBase64String(outputBytes);
    }

    public static double DecryptDouble(string input, ICryptoTransform decryptor) {
        byte[] inputBytes = Convert.FromBase64String(input);
        byte[] outputBytes = decryptor.TransformFinalBlock(inputBytes, 0, inputBytes.Length);
        return BitConverter.ToDouble(outputBytes, 0);
    }

    public static string EncryptColor(Color input, ICryptoTransform encryptor) {
        string color = input.r + "," + input.g + "," + input.b + "," + input.a;
        return EncryptString(color, encryptor);
    }

    public static Color DecryptColor(string input, ICryptoTransform decryptor) {
        string color = DecryptString(input, decryptor);
        string[] values = color.Split(',');
        if (values.Length != 4) {
            Debug.LogWarning("Invalid color string: " + color);
            return default(Color);
        }
        return new Color(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]), float.Parse(values[3]));
    }

    public static string EncryptVector2(Vector2 input, ICryptoTransform encryptor) {
        string v2 = input.x + "," + input.y;
        return EncryptString(v2, encryptor);
    }

    public static Vector2 DecryptVector2(string input, ICryptoTransform decryptor) {
        string v2 = DecryptString(input, decryptor);
        string[] values = v2.Split(',');
        if (values.Length != 2) {
            Debug.LogWarning("Invalid vector2 string: " + v2);
            return default(Vector2);
        }
        return new Vector2(float.Parse(values[0]), float.Parse(values[1]));
    }

    public static string EncryptVector3(Vector3 input, ICryptoTransform encryptor) {
        string v3 = input.x + "," + input.y + "," + input.z;
        return EncryptString(v3, encryptor);
    }

    public static Vector3 DecryptVector3(string input, ICryptoTransform decryptor) {
        string v3 = DecryptString(input, decryptor);
        string[] values = v3.Split(',');
        if (values.Length != 3) {
            Debug.LogWarning("Invalid vector3 string: " + v3);
            return default(Vector3);
        }
        return new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]));
    }

    public static string EncryptRect(Rect input, ICryptoTransform encryptor) {
        string rect = input.x + "," + input.y + "," + input.width + "," + input.height;
        return EncryptString(rect, encryptor);
    }

    public static Rect DecryptRect(string input, ICryptoTransform decryptor) {
        string rect = DecryptString(input, decryptor);
        string[] values = rect.Split(',');
        if (values.Length != 4) {
            Debug.LogWarning("Invalid rect string: " + rect);
            return default(Rect);
        }
        return new Rect(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]), float.Parse(values[3]));
    }

    public static string EncryptBounds(Bounds input, ICryptoTransform encryptor) {
        string center = input.center.x + "," + input.center.y + "," + input.center.z;
        string size = input.size.x + "," + input.size.y + "," + input.size.z;
        string bounds = center + ";" + size;
        return EncryptString(bounds, encryptor);
    }

    public static Bounds DecryptBounds(string input, ICryptoTransform decryptor) {
        string bounds = DecryptString(input, decryptor);
        string[] values = bounds.Split(';');
        if (values.Length != 2) {
            Debug.LogWarning("Invalid bounds string: " + bounds);
            return default(Bounds);
        }
        string[] center = values[0].Split(',');
        string[] size = values[1].Split(',');
        if (center.Length != 3 || size.Length != 3) {
            Debug.LogWarning("Invalid bounds string: " + bounds);
            return default(Bounds);
        }
        return new Bounds(new Vector3(float.Parse(center[0]), float.Parse(center[1]), float.Parse(center[2])), 
            new Vector3(float.Parse(size[0]), float.Parse(size[1]), float.Parse(size[2])));
    }

    #region Default
    private static RijndaelManaged defaultRijndael {
        get {
            if (_defaultRijndael == null) {
                _defaultRijndael = CreateSimpleRijndael(defaultPassword);
            }
            return _defaultRijndael;
        }
    }
    private static RijndaelManaged _defaultRijndael;

    private static RijndaelManaged CreateSimpleRijndael(string password) {
        RijndaelManaged rijndael = new RijndaelManaged();
        rijndael.Key = Encoding.ASCII.GetBytes(password);
        rijndael.Mode = CipherMode.ECB;
        return rijndael;
    }

    private static ICryptoTransform defaultEncryptor {
        get {
            if (_defaultEncryptor == null) {
                _defaultEncryptor = defaultRijndael.CreateEncryptor();
            }
            return _defaultEncryptor;
        }
    }
    private static ICryptoTransform _defaultEncryptor;

    private static ICryptoTransform defaultDecryptor {
        get {
            if (_defaultDecryptor == null) {
                _defaultDecryptor = defaultRijndael.CreateDecryptor();
            }
            return _defaultDecryptor;
        }
    }
    private static ICryptoTransform _defaultDecryptor;

    public static string EncryptString(string input) {
        return EncryptString(input, defaultEncryptor);
    }

    public static string DecryptString(string input) {
        return DecryptString(input, defaultDecryptor);
    }

    public static string EncryptBool(bool input) {
        return EncryptBool(input, defaultEncryptor);
    }

    public static bool DecryptBool(string input) {
        return DecryptBool(input, defaultDecryptor);
    }

    public static string EncryptChar(char input) {
        return EncryptChar(input, defaultEncryptor);
    }

    public static char DecryptChar(string input) {
        return DecryptChar(input, defaultDecryptor);
    }

    public static string EncryptShort(short input) {
        return EncryptShort(input, defaultEncryptor);
    }

    public static short DecryptShort(string input) {
        return DecryptShort(input, defaultDecryptor);
    }

    public static string EncryptInt(int input) {
        return EncryptInt(input, defaultEncryptor);
    }

    public static int DecryptInt(string input) {
        return DecryptInt(input, defaultDecryptor);
    }

    public static string EncryptLong(long input) {
        return EncryptLong(input, defaultEncryptor);
    }

    public static long DecryptLong(string input) {
        return DecryptLong(input, defaultDecryptor);
    }

    public static string EncryptFloat(float input) {
        return EncryptFloat(input, defaultEncryptor);
    }

    public static float DecryptFloat(string input) {
        return DecryptFloat(input, defaultDecryptor);
    }

    public static string EncryptDouble(double input) {
        return EncryptDouble(input, defaultEncryptor);
    }

    public static double DecryptDouble(string input) {
        return DecryptDouble(input, defaultDecryptor);
    }

    public static string EncryptColor(Color input) {
        return EncryptColor(input, defaultEncryptor);
    }

    public static Color DecryptColor(string input) {
        return DecryptColor(input, defaultDecryptor);
    }

    public static string EncryptVector2(Vector2 input) {
        return EncryptVector2(input, defaultEncryptor);
    }

    public static Vector2 DecryptVector2(string input) {
        return DecryptVector2(input, defaultDecryptor);
    }

    public static string EncryptVector3(Vector3 input) {
        return EncryptVector3(input, defaultEncryptor);
    }

    public static Vector3 DecryptVector3(string input) {
        return DecryptVector3(input, defaultDecryptor);
    }

    public static string EncryptRect(Rect input) {
        return EncryptRect(input, defaultEncryptor);
    }

    public static Rect DecryptRect(string input) {
        return DecryptRect(input, defaultDecryptor);
    }

    public static string EncryptBounds(Bounds input) {
        return EncryptBounds(input, defaultEncryptor);
    }

    public static Bounds DecryptBounds(string input) {
        return DecryptBounds(input, defaultDecryptor);
    }
    #endregion

    #region AES
    private const int AES_KEY_SIZE = 16;
    //private const string AES_SALT_KEY = "ShMG8hLyZ7k~Ge5@";
    private const string AES_SALT_KEY = "Www.Ezergame.Com";
    // TODO: It's strongy recommended to generate random VI each encryption (and store it with encrypted value)
    private const string AES_RGBIV_KEY = "~6YUi0Sv5@|{aOZO";

    private static ICryptoTransform aesEncryptor {
        get {
            if (_aesEncryptor == null) {
                _aesEncryptor = CreateAESEncryptor(defaultPassword);
            }
            return _aesEncryptor;
        }
    }
    private static ICryptoTransform _aesEncryptor;

    private static ICryptoTransform aesDecryptor {
        get {
            if (_aesDecryptor == null) {
                _aesDecryptor = CreateAESDecryptor(defaultPassword);
            }
            return _aesDecryptor;
        }
    }
    private static ICryptoTransform _aesDecryptor;

    public static ICryptoTransform CreateAESEncryptor(string password, 
        int keySize = AES_KEY_SIZE, string salt = AES_SALT_KEY, string rgbIV = AES_RGBIV_KEY) {
        var keyBytes = new Rfc2898DeriveBytes(password, Encoding.UTF8.GetBytes(salt)).GetBytes(keySize);
        var aes = new RijndaelManaged { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
        return aes.CreateEncryptor(keyBytes, Encoding.UTF8.GetBytes(rgbIV));
    }

    public static ICryptoTransform CreateAESDecryptor(string password,
        int keySize = AES_KEY_SIZE, string salt = AES_SALT_KEY, string rgbIV = AES_RGBIV_KEY) {
        var keyBytes = new Rfc2898DeriveBytes(password, Encoding.UTF8.GetBytes(salt)).GetBytes(keySize);
        var aes = new RijndaelManaged { Mode = CipherMode.CBC, Padding = PaddingMode.None };
        return aes.CreateDecryptor(keyBytes, Encoding.UTF8.GetBytes(rgbIV));
    }

    public static string EncryptByAES(string value) {
        return EncryptByAES(Encoding.UTF8.GetBytes(value), defaultPassword);
    }

    public static string EncryptByAES(string value, string password) {
        return EncryptByAES(Encoding.UTF8.GetBytes(value), password);
    }

    public static string EncryptByAES(byte[] value) {
        return Convert.ToBase64String(aesEncryptor.TransformFinalBlock(value, 0, value.Length));
    }

    public static string EncryptByAES(byte[] value, string password) {
        var encryptor = CreateAESEncryptor(password);
        return Convert.ToBase64String(encryptor.TransformFinalBlock(value, 0, value.Length));
    }

    public static string DecryptByAES(string value) {
        return DecryptByAES(Convert.FromBase64String(value), defaultPassword);
    }

    public static string DecryptByAES(string value, string password) {
        return DecryptByAES(Convert.FromBase64String(value), password);
    }

    public static string DecryptByAES(byte[] value) {
        return Encoding.UTF8.GetString(aesDecryptor.TransformFinalBlock(value, 0, value.Length));
    }

    public static string DecryptByAES(byte[] value, string password) {
        var decryptor = CreateAESDecryptor(password);
        return Encoding.UTF8.GetString(decryptor.TransformFinalBlock(value, 0, value.Length));
    }
    #endregion

    #region Base64
    public static string EncodeByBase64(string value) {
        return Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
    }

    public static string DecodeByBase64(string value) {
        return Encoding.UTF8.GetString(Convert.FromBase64String(value));
    }

    public static void ReverseChars(IList<char> chars) {
        for (var i = 1; i < chars.Count; i += 2) {
            var c = chars[i];
            chars[i] = chars[i - 1];
            chars[i - 1] = c;
        }
    }

    public static string ReverseString(string value) {
        var chars = value.ToCharArray();
        ReverseChars(chars);
        return new string(chars);
    }

    /// <summary>
    /// Encrypt plain string
    /// </summary>
    public static string EncryptByB64Reverse(string value) {
        var base64 = EncodeByBase64(value);
        return ReverseString(base64);
    }

    /// <summary>
    /// Decode protected string.
    /// </summary>
    public static string DecryptByB64Reverse(string value) {
        string reversed = ReverseString(value);
        return DecodeByBase64(reversed);
    }

    public static byte[] XOR(byte[] bytes, byte[] key) {
        for (int i = 0, j = 0; i < bytes.Length; ++i, ++j) {
            j %= key.Length;
            bytes[i] ^= key[j];
        }
        return bytes;
    }

    public static string EncryptByB64XOR(string value, string key) {
        return Convert.ToBase64String(XOR(Encoding.UTF8.GetBytes(value), Encoding.UTF8.GetBytes(key)));
    }

    public static string EncryptByB64XOR(string value) {
        return EncryptByB64XOR(value, defaultPassword);
    }

    public static string DecryptByB64XOR(string value, string key) {
        return Encoding.UTF8.GetString(XOR(Convert.FromBase64String(value), Encoding.UTF8.GetBytes(key)));
    }

    public static string DecryptByB64XOR(string value) {
        return DecryptByB64XOR(value, defaultPassword);
    }
    #endregion
}
