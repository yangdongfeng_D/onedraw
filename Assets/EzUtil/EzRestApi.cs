﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzRestApi {

	private static IEnumerator WaitForRequest(WWW www, 
		System.Action<string> onSuccess, 
		System.Action<string> onError) {
		Debug.Log("Requesting url: " + www.url);
		yield return www;
		if (!string.IsNullOrEmpty(www.error)) {
			Debug.LogWarning("Request error: " + www.error + " of url: " + www.url);
			if (onError != null) {
				onError.Invoke(www.error);
			}
		} else if (onSuccess != null) {
			Debug.Log("Request success: " + www.text);
			onSuccess.Invoke(www.text);
		}
	}

	private static IEnumerator WaitForRequest(WWW www, float timeout, 
		System.Action<string> onSuccess, 
		System.Action<string> onError, 
		System.Action onTimeout) {
		Debug.Log("Requesting url: " + www.url);
		bool isTimeout = false;
		float time = Time.realtimeSinceStartup;
		while (!www.isDone) {
			if (Time.realtimeSinceStartup - time > timeout) {
				isTimeout = true;
				Debug.LogWarning("Reqest timeout: " + time + " seconds of url: " + www.url);
				if (onTimeout != null) {
					onTimeout.Invoke();
				}
				www.Dispose();
				break;
			}
			yield return null;
		}
		if (!isTimeout) {
			if (!string.IsNullOrEmpty(www.error)) {
				Debug.LogWarning("Request error: " + www.error);
				if (onError != null) {
					onError.Invoke(www.error);
				}
			} else if (onSuccess != null) {
				Debug.Log("Request success: " + www.text);
				onSuccess.Invoke(www.text);
			}
		}
	}

	public static IEnumerator Get(string url, System.Action<string> onSuccess = null, System.Action<string> onError = null) {
		WWW www = new WWW(url);
		yield return WaitForRequest(www, onSuccess, onError);
	}

	public static IEnumerator Get(string url, float timeout, System.Action<string> onSuccess = null, System.Action<string> onError = null, System.Action onTimeout = null) {
		WWW www = new WWW(url);
		yield return WaitForRequest(www, timeout, onSuccess, onError, onTimeout);
	}

	public static IEnumerator Post(string url, System.Action<string> onSuccess = null, System.Action<string> onError = null) {
        WWWForm form = new WWWForm();
        form.AddField("url", url);
        WWW www = new WWW(url, form);
        yield return WaitForRequest(www, onSuccess, onError);
	}

	public static IEnumerator Post(string url, IDictionary<string, string> args, System.Action<string> onSuccess = null, System.Action<string> onError = null) {
		WWWForm form = new WWWForm();
		foreach (var pair in args) {
			form.AddField(pair.Key, pair.Value);
		}
		WWW www = new WWW(url, form);
		yield return WaitForRequest(www, onSuccess, onError);
	}

	public static IEnumerator Post(string url, byte[] data, System.Action<string> onSuccess = null, System.Action<string> onError = null) {
		WWW www = new WWW(url, data);
		yield return WaitForRequest(www, onSuccess, onError);
	}

	public static IEnumerator Post(string url, byte[] data, Dictionary<string, string> headers, System.Action<string> onSuccess = null, System.Action<string> onError = null) {
		WWW www = new WWW(url, data, headers);
		yield return WaitForRequest(www, onSuccess, onError);
	}
}
