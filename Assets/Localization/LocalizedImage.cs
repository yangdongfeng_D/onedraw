﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LocalizedImage : MonoBehaviour {

    public string[] languages;
    public Sprite[] sprites;

    private Dictionary<string, Sprite> dict;

	// Use this for initialization
	void Start() {
        dict = new Dictionary<string, Sprite>();
        for (int i = 0; i < languages.Length; ++i) {
            dict.Add(languages[i], sprites[i]);
        }
        string language = Localization.GetLanguage();
        if (dict.ContainsKey(language)) {
            Image image = GetComponent<Image>();
            if (image != null) {
                image.sprite = dict[language];
            } else {
                SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
                if (spriteRenderer != null) {
                    spriteRenderer.sprite = dict[language];
                }
            }
        }
    }
}
