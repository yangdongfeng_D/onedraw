﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTransform : MonoBehaviour {

    private Vector3 localPosition;
    private Quaternion localRotation;
    private Vector3 localScale;
    private Color color;

    // Use this for initialization
    void Awake() {
        localPosition = transform.localPosition;
        localRotation = transform.localRotation;
        localScale = transform.localScale;
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr != null) {
            color = sr.color;
        } else {
            Renderer r = GetComponent<Renderer>();
            if (r != null && r.material.HasProperty("_Color")) {
                color = r.material.color;
            }
        }
    }
	
	void OnDisable() {
        transform.localPosition = localPosition;
        transform.localRotation = localRotation;
        transform.localScale = localScale;
        Rigidbody body = GetComponent<Rigidbody>();
        if (body != null) {
            body.velocity = Vector3.zero;
            body.Sleep();
        } else {
            Rigidbody2D body2D = GetComponent<Rigidbody2D>();
            if (body2D != null) {
                body2D.velocity = Vector2.zero;
                body2D.Sleep();
            }
        }
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr != null) {
            sr.color = color;
        } else {
            Renderer r = GetComponent<Renderer>();
            if (r != null && r.material.HasProperty("_Color")) {
                r.material.color = color;
            }
        }
    }
}
