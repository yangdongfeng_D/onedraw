﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPanel : Panel {

    private Transform noAdsButton;

    protected override void OnAwake() {
        base.OnAwake();
        noAdsButton = transform.Find("NoAds");
    }

    protected override void OnUpdate() {
        base.OnUpdate();
		if (noAdsButton != null) {
        	noAdsButton.gameObject.SetActive(!GameManager.instance.noAds);
		}
    }

}
