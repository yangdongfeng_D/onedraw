﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CollisionEnter2DEvent : UnityEvent<Collision2D> { }

[System.Serializable]
public class CollisionStay2DEvent : UnityEvent<Collision2D> { }

[System.Serializable]
public class CollisionExit2DEvent : UnityEvent<Collision2D> { }

public class Collision2DHandler : MonoBehaviour {

    public CollisionEnter2DEvent onCollisionEnter2D;
    [SerializeField]
    public LayerMask enterLayerMask;

    public CollisionStay2DEvent onCollisionStay2D;
    [SerializeField]
    public LayerMask stayLayerMask;

    public CollisionExit2DEvent onCollisionExit2D;
    [SerializeField]
    public LayerMask exitLayerMask;

    void OnCollisionEnter2D(Collision2D collision) {
        if (onCollisionEnter2D != null && ((1 << collision.gameObject.layer) & enterLayerMask.value) != 0) {
            onCollisionEnter2D.Invoke(collision);
        }
    }

    void OnCollisionStay2D(Collision2D collision) {
        if (onCollisionStay2D != null && ((1 << collision.gameObject.layer) & stayLayerMask.value) != 0) {
            onCollisionStay2D.Invoke(collision);
        }
    }

    void OnCollisionExit2D(Collision2D collision) {
        if (onCollisionExit2D != null && ((1 << collision.gameObject.layer) & exitLayerMask.value) != 0) {
            onCollisionExit2D.Invoke(collision);
        }
    }
}
