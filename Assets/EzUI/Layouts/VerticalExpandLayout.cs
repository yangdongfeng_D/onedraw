﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalExpandLayout : MonoBehaviour {

    public float shrinkHeight;

    private RectTransform rectTransform;
    private float expandHeight;

    void Awake() {
        rectTransform = GetComponent<RectTransform>();
    }

	// Use this for initialization
	void Start() {
        expandHeight = rectTransform.sizeDelta.y;
    }

    public void SetExpanded(bool expanded) {
        if (expanded) {
            Expand();
        } else {
            Shrink();
        }
    }

    public void Shrink() {
        ChangeHeightTo(shrinkHeight);
    }

    public void Expand() {
        ChangeHeightTo(expandHeight);
    }

    public void ChangeHeightTo(float height, float time = 0) {
        iTween.ValueTo(gameObject, iTween.Hash("from", rectTransform.sizeDelta.y, "to", height,
            "time", time, "easetype", iTween.EaseType.easeInBack,
            "onupdate", "OnHeightUpdate"));
    }

    void OnHeightUpdate(float height) {
        rectTransform.sizeDelta = rectTransform.sizeDelta.NewY(height);
    }
}
