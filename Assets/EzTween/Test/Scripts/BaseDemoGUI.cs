using UnityEngine;
using System;
using System.Collections;



public class BaseDemoGUI : MonoBehaviour
{
	protected EzTweenBase _tween;
	protected float _tweenTimeScale = 1;

	
	protected virtual void OnGUI()
	{
		if( _tween == null )
			return;
		
		GUILayout.Label( "elapsed: " + string.Format( "{0:0.##}", _tween.totalElapsedTime ) );
		
		
		if( GUILayout.Button( "play" ) )
			_tween.Play();
		
		if( GUILayout.Button( "pause" ) )
			_tween.Pause();
		
		if( GUILayout.Button( "reverse" ) )
			_tween.Reverse();
		
		if( GUILayout.Button( "restart" ) )
			_tween.Restart();
		
		if( GUILayout.Button( "play backwards" ) )
			_tween.PlayBackward();
		
		if( GUILayout.Button( "play forward" ) )
			_tween.PlayForward();
		
		if( GUILayout.Button( "complete" ) )
			_tween.Complete();
		
		GUILayout.Label( "Time Scale: " + string.Format( "{0:0.##}", _tween.timeScale ) );
		var newTweenTimeScale = GUILayout.HorizontalSlider( _tweenTimeScale, 0, 3 );
		if( newTweenTimeScale != _tweenTimeScale )
		{
			_tweenTimeScale = newTweenTimeScale;
			_tween.timeScale = _tweenTimeScale;
		}
		
		easeTypesGUI();
	}
	
	
	protected void easeTypesGUI()
	{
		// ease section. only available for Tweens
		if( _tween is EzTween )
		{
			GUILayout.BeginArea( new Rect( Screen.width - 200, 0, 100, Screen.height ) );
			
			GUILayout.Label( "Ease Types" );
			
			var allEaseTypes = Enum.GetValues( typeof( EzEaseType ) );
			var midway = Mathf.Round( allEaseTypes.Length / 2 );
			
			for( var i = 0; i < allEaseTypes.Length; i++ )
			{
				var ease = allEaseTypes.GetValue( i );
				
				
				if( i == midway )
				{
					GUILayout.EndArea();
					GUILayout.BeginArea( new Rect( Screen.width - 100, 0, 100, Screen.height ) );
				}
				
				if( GUILayout.Button( ease.ToString() ) )
					((EzTween)_tween).easeType = (EzEaseType)ease;
			}
			
			GUILayout.EndArea();
		}
	}

}
