﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShareVirusPanel : Panel {

	protected override void OnStart() {
		base.OnStart();
		transform.Find("InnerPanel/Title").GetComponent<Text>().text = EzRemoteConfig.shareVirusTitle;
		transform.Find("InnerPanel/Body").GetComponent<Text>().text = EzRemoteConfig.shareVirusBody;
		transform.Find("InnerPanel/Remark").GetComponent<Text>().text = EzRemoteConfig.shareVirusRemark;
        transform.Find("InnerPanel/Share/Text").GetComponent<Text>().text = EzRemoteConfig.shareVirusButton;
        transform.Find("InnerPanel/URL").GetComponent<InputField>().text = GameManager.instance.shareVirusUrl;
        transform.Find("InnerPanel/Total/Value").GetComponent<InputField>().text = GameManager.instance.shareVirusResult.total.ToString();
        transform.Find("InnerPanel/Today/Value").GetComponent<InputField>().text = GameManager.instance.shareVirusResult.today.ToString();
    }

    public void Share() {
		OnClick("Share");
		Close();
		GameManager.instance.ShareVirus();
	}

    public void CopyURL() {
        EzClipboard.SetText(GameManager.instance.shareVirusUrl);
    }
}
