﻿public interface AdSource {

    string name { get; }

    void Initialize();

    bool IsInterstitialReady();

    bool ShowInterstitial(System.Action onClose = null);

    bool IsRewardedVideoReady();

    bool ShowRewardedVideo(System.Action onRewarded, System.Action onCanceled = null);

    void OnPause();

    void OnResume();
}
