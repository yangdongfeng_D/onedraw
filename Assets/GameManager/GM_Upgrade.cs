﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public void OpenUpgrade() {
        if (string.IsNullOrEmpty(EzRemoteConfig.upgradeUrl)) return;
        Panel.Open("UpgradePanel");
    }

    public void GotoUpgrade() {
        if (string.IsNullOrEmpty(EzRemoteConfig.upgradeUrl)) return;
        EzAnalytics.LogEvent("Upgrade", "Goto");
        Application.OpenURL(EzRemoteConfig.upgradeUrl);
    }

}
