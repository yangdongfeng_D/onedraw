﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteTile : MonoBehaviour {

    public float tilesX = 1;
    public float tilesY = 1;

    private SpriteRenderer spriteRenderer;

    void OnEnable() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        UpdateTiles(true);
    }

    void OnDisable() {
        UpdateTiles(false);
    }

    void Update() {
        UpdateTiles(true);
    }

    void UpdateTiles(bool enabled) {
        Vector3 size = spriteRenderer.sprite.bounds.size;
        float x = enabled ? tilesX : 1;
        float y = enabled ? tilesY : 1;
        transform.localScale = new Vector3(size.x * x, size.y * y, 1);

        MaterialPropertyBlock mpb = new MaterialPropertyBlock();
        spriteRenderer.GetPropertyBlock(mpb);
        mpb.SetFloat("_RepeatX", x);
        mpb.SetFloat("_RepeatY", y);
        spriteRenderer.SetPropertyBlock(mpb);
    }
}
