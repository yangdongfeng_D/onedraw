﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicButton : MonoBehaviour {

    public Sprite on;
    public Sprite off;

    public Image icon;

    void Start() {
        GetComponent<Button>().onClick.AddListener(OnClick);
        UpdateIcon();
    }

    void OnClick() {
        GameManager.instance.muteMusic = !GameManager.instance.muteMusic;
        UpdateIcon();
    }

    void UpdateIcon() {
        icon.sprite = GameManager.instance.muteMusic ? off : on;
    }
}
