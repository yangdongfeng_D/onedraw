﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WebImage : MonoBehaviour {

	[System.Serializable]
	public class LoadEvent : UnityEvent<Sprite> {

	}

	public string url;
	public bool loadOnAwake = true;
	public Sprite placeholder;
	public bool preserveAspect;
	public bool useNativeSize;
	public LoadEvent onLoaded;

	private Image image;

	void Awake() {
		image = GetComponent<Image>();
		if (image == null) {
			image = gameObject.AddComponent<Image>();
		}
		if (placeholder != null) {
			UpdateSprite(placeholder);
		}
		if (loadOnAwake) {
			Load();
		}
	}

	public void Load() {
		if (string.IsNullOrEmpty(url)) return;
		Debug.Log("Load image by url: " + url);
		StartCoroutine(EzImageLoader.LoadSprite(url, (sprite) => {
			Debug.Log("Image loaded: " + url);
			UpdateSprite(sprite);
			if (onLoaded != null) {
				onLoaded.Invoke(sprite);
			}
		}));
	}

	public void Load(string url) {
		this.url = url;
		Load();
	}

	public void UpdateSprite(Sprite sprite) {
		image.sprite = sprite;
		image.preserveAspect = preserveAspect;
		if (useNativeSize) {
			image.SetNativeSize();
		}
	}
}
