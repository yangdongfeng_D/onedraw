﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameManager : MonoBehaviour {

    public int testCoins = -1;
    public int initCoins = 1000;

    private const string PLAY_TIMES = "PLAY_TIMES";
    private const string NAME = "NAME";
    private const string COINS = "COINS";
    private const string SCORE = "SCORE";
    private const string BEST_SCORE = "BEST_SCORE";
    private const string UNLOCKED_LEVELS = "UNLOCKED_LEVELS";
    private const string SCORE_OF_LEVEL = "SCORE_OF_LEVEL";
    private const string STAR_OF_LEVEL = "STAR_OF_LEVEL";
    private const string TIME_OF_LEVEL = "TIME_OF_LEVEL";
    private const string COST_OF_LEVEL = "COST_OF_LEVEL";
    private const string VIBRATE = "VIBRATE";
    private const string NO_ADS = "NO_ADS";
    public bool noAds {
        get { return EzPrefs.GetBool(NO_ADS); }
        set { EzPrefs.SetBool(NO_ADS, value); }
    }
    public int playTimes;

    private const string GUIDED = "GUIDED";
    public bool guided {
        get { return EzPrefs.GetBool(GUIDED); }
        set { EzPrefs.SetBool(GUIDED, value); }
    }

    public string playerName;
    public Sprite playerAvatar;
    public int coins;
    public int score;
    public int bestScore;
    public int unlockedLevels;

    void LoadPlayerData() {
        playTimes = EzPrefs.GetInt(PLAY_TIMES, 0);
        EzPrefs.SetInt(PLAY_TIMES, ++playTimes);
        playerName = EzPrefs.GetString(NAME, playerName);
        coins = testCoins >= 0 ? testCoins : EzPrefs.GetInt(COINS, initCoins);
        score = EzPrefs.GetInt(SCORE, 0);
        bestScore = EzPrefs.GetInt(BEST_SCORE, 0);
        unlockedLevels = testLevels > 0 ? testLevels : EzPrefs.GetInt(UNLOCKED_LEVELS, 1);
    }

    public void FixupPlayerName() {
        if (string.IsNullOrEmpty(playerName)) {
            UpdatePlayerName(Localization.GetText("Guest") + Random.Range(123456789, 987654321));
        }
    }

    public void UpdatePlayerName(string name) {
        playerName = name;
        EzPrefs.SetString(NAME, playerName);
    }

    public void GainCoins(int gains, string from = "") {
        coins += gains;
        EzPrefs.SetInt(COINS, coins);
        EzAnalytics.LogEvent("Coin", "Gain", from, gains);
    }

    public int CostCoins(int costs, string to = "") {
        if (costs > coins) return -1;
        coins -= costs;
        EzPrefs.SetInt(COINS, coins);
        EzAnalytics.LogEvent("Coin", "Cost", to, costs);
        return coins;
    }

    public void GainScore(int gains) {
        UpdateScore(score + gains);
    }

    public bool UpdateScore(int score) {
        this.score = score;
        EzPrefs.SetInt(SCORE, score);
        if (score > bestScore) {
            bestScore = score;
            EzPrefs.SetInt(BEST_SCORE, bestScore);
            return true;
        }
        return false;
    }

    public bool UnlockLevel() {
        if (unlockedLevels < maxLevels) {
            ++unlockedLevels;
            EzPrefs.SetInt(UNLOCKED_LEVELS, unlockedLevels);
            return true;
        }
        return false;
    }

    public int GetScoreOfCurrentLevel() {
        return GetScoreOfLevel(currentLevel);
    }

    public void UpdateScoreOfCurrentLevel(int score) {
        UpdateScoreOfLevel(currentLevel, score);
    }

    public int GetScoreOfLevel(int levelId) {
        return EzPrefs.GetInt(SCORE_OF_LEVEL + "_" + levelId);
    }

    public void UpdateScoreOfLevel(int levelId, int score) {
        string key = SCORE_OF_LEVEL + "_" + levelId;
        if (score > EzPrefs.GetInt(key)) {
            EzPrefs.SetInt(key, score);
        }
    }

    public int GetStarOfCurrentLevel() {
        return GetStarOfLevel(currentLevel);
    }

    public void UpdateStarOfCurrentLevel(int star) {
        UpdateStarOfLevel(currentLevel, star);
    }

    public int GetStarOfLevel(int levelId) {
        return EzPrefs.GetInt(STAR_OF_LEVEL + "_" + levelId);
    }

    public void UpdateStarOfLevel(int levelId, int star) {
        string key = STAR_OF_LEVEL + "_" + levelId;
        if (star > EzPrefs.GetInt(key)) {
            EzPrefs.SetInt(key, star);
        }
    }

    public float GetTimeOfCurrentLevel() {
        return GetTimeOfLevel(currentLevel);
    }

    public void UpdateTimeOfCurrentLevel(float time) {
        UpdateTimeOfLevel(currentLevel, time);
    }

    public float GetTimeOfLevel(int levelId) {
        return EzPrefs.GetFloat(TIME_OF_LEVEL + "_" + levelId, float.PositiveInfinity);
    }

    public void UpdateTimeOfLevel(int levelId, float time) {
        string key = TIME_OF_LEVEL + "_" + levelId;
        if (time < EzPrefs.GetFloat(key)) {
            EzPrefs.SetFloat(key, time);
        }
    }

    public int GetCostOfCurrentLevel() {
        return GetCostOfLevel(currentLevel);
    }

    public void UpdateCostOfCurrentLevel(int cost) {
        UpdateCostOfLevel(currentLevel, cost);
    }

    public int GetCostOfLevel(int levelId) {
        return EzPrefs.GetInt(COST_OF_LEVEL + "_" + levelId, int.MaxValue);
    }

    public void UpdateCostOfLevel(int levelId, float time) {
        string key = COST_OF_LEVEL + "_" + levelId;
        if (time < EzPrefs.GetFloat(key)) {
            EzPrefs.SetFloat(key, time);
        }
    }

    public bool vibrateEnabled {
        get { return EzPrefs.GetInt(VIBRATE, 1) != 0; }
        set { EzPrefs.SetInt(VIBRATE, value ? 1 : 0); }
    }

    public void Vibrate() {
#if UNITY_ANDROID || UNITY_IOS
        if (vibrateEnabled) Handheld.Vibrate();
#endif
    }

}
