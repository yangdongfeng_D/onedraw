using UnityEngine;
using System.Collections;


public class EzTweenShake : TweenProperty {
	private Transform target;
	private Vector3 shakeMagnitude;
	
	private Vector3 originalEndValue;
	private Vector3 startPosition;
	private Vector3 startRotation;
	private Vector3 startScale;
	
	private EzShakeType shakeType;
	private int frameCount;
	private int frameMod;
	private bool localSpace;

	public bool LocalSpace { get { return localSpace; } }
	
	/// <summary>
	/// you can shake any combination of position, scale and eulers by passing in a bitmask of the types you want to shake. frameMod
	/// allows you to specify what frame count the shakes should occur on. for example, a frameMod of 3 would mean that only when
	/// frameCount % 3 == 0 will the shake occur
	/// </summary>
	public EzTweenShake(Vector3 shakeMagnitude, EzShakeType shakeType, int frameMod = 1, bool localSpace = false)
		: base(true) {
		this.shakeMagnitude = shakeMagnitude;
		this.shakeType = shakeType;
		this.frameMod = frameMod;
		this.localSpace = localSpace;
	}

	
	#region Object overrides

	public override int GetHashCode() {
		return base.GetHashCode();
	}
	
	public override bool Equals(object obj) {
		// start with a base check and then compare our material names
		return (base.Equals(obj) && this.shakeType == ((EzTweenShake)obj).shakeType);
	}

	#endregion

	
	public override bool ValidateTarget(object target) {
		return target is Transform;
	}
	
	public override void Start() {
		target = owner.target as Transform;
		frameCount = 0;

		// store off any properties we will be shaking
		if ((shakeType & EzShakeType.Position) != 0) {
			startPosition = localSpace ? target.localPosition : target.position;
		}
		if ((shakeType & EzShakeType.Rotation) != 0) {
			startRotation = localSpace ? target.localEulerAngles : target.eulerAngles;
		}
		if ((shakeType & EzShakeType.Scale) != 0) {
			startScale = target.localScale;
		}
	}
	
	private Vector3 RandomDiminishingTarget(float falloffValue) {
		return new Vector3(
			Random.Range(-shakeMagnitude.x, shakeMagnitude.x) * falloffValue,
			Random.Range(-shakeMagnitude.y, shakeMagnitude.y) * falloffValue,
			Random.Range(-shakeMagnitude.z, shakeMagnitude.z) * falloffValue
		);
	}

	public override void Tick(float elapsed) {
		// should we skip any frames?
		if (frameMod > 1 && ++frameCount % frameMod != 0)
			return;
		
		// we want 1 minus the eased time so that we go from 1 - 0 for a shake
		var easedTime = 1 - easeFunction(elapsed, 0, 1, owner.duration);
		
		// shake any properties required
		if ((shakeType & EzShakeType.Position) != 0) {
			var val = startPosition + RandomDiminishingTarget(easedTime);
			if (localSpace) {
				target.localPosition = val;
			} else {
				target.position = val;
			}
		}
		if ((shakeType & EzShakeType.Rotation) != 0) {
			var val = startRotation + RandomDiminishingTarget(easedTime);
			if (localSpace) {
				target.localEulerAngles = val;
			} else {
				target.eulerAngles = val;
			}
		}
		if ((shakeType & EzShakeType.Scale) != 0) {
			target.localScale = startScale + RandomDiminishingTarget(easedTime);
		}
	}

}
