﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePanel : Panel {

	protected override void OnStart() {
		base.OnStart();
		transform.Find("InnerPanel/Title").GetComponent<Text>().text = EzRemoteConfig.upgradeTitle;
		transform.Find("InnerPanel/Body").GetComponent<Text>().text = EzRemoteConfig.upgradeBody;
	}

	public void Upgrade() {
		OnClick("Upgrade");
		GameManager.instance.GotoUpgrade();
		if (!EzRemoteConfig.upgradeForce) {
			Close();
		}
	}

	public void Later() {
		Cancel();
		if (EzRemoteConfig.upgradeForce) {
			Application.Quit();
		}
	}
}
