﻿using System;
using UnityEngine;

public abstract class EzTweenMaterialProperty<T, A> : TweenProperty where A : ITweenable<T> {

    public string Name { get; protected set; }
    protected Material target;

    protected A tweenable;

    protected T originalEndValue;
    protected T startValue;
    protected T endValue;
    protected T diffValue;

    protected EzTweenMaterialProperty(string name, T endValue, bool relative = false)
        : base(relative) {
        Name = name;
        originalEndValue = endValue;
        tweenable = Activator.CreateInstance<A>();
    }

    public override string ToString() {
        return GetType().Name + "." + Name;
    }

    public override bool ValidateTarget(object target) {
        return (target is Material || target is GameObject || target is Transform || target is Renderer);
    }

    public override void Init(EzTween owner) {
        base.Init(owner);
        if (owner.target is Material)
            target = (Material)owner.target;
        else if (owner.target is GameObject)
            target = ((GameObject)owner.target).GetComponent<Renderer>().material;
        else if (owner.target is Transform)
            target = ((Transform)owner.target).GetComponent<Renderer>().material;
        else if (owner.target is Renderer)
            target = ((Renderer)owner.target).material;
    }

    public override void Start() {
        endValue = originalEndValue;

        if (owner.fromMode) {
            startValue = endValue;
            endValue = Get();
        } else {
            startValue = Get();
        }

        if (relative && !owner.fromMode) {
            diffValue = endValue;
        } else {
            diffValue = tweenable.Diff(startValue, endValue);
        }
    }

    public override void Tick(float elapsed) {
        var easedTime = easeFunction(elapsed, 0, 1, owner.duration);
        var result = tweenable.Lerp(startValue, diffValue, easedTime);
        Set(result);
    }

    protected abstract T Get();

    protected abstract void Set(T value);
}

public class EzTweenMaterialColor : EzTweenMaterialProperty<Color, ColorTweenable> {
    public EzTweenMaterialColor(Color endValue, bool relative = false)
        : this("_Color", endValue, relative) {
    }

    public EzTweenMaterialColor(string name, Color endValue, bool relative = false)
        : base(name, endValue, relative) {
    }

    protected override Color Get() {
        return target.GetColor(Name);
    }

    protected override void Set(Color value) {
        target.SetColor(Name, value);
    }
}

public class EzTweenMaterialInt : EzTweenMaterialProperty<int, IntTweenable> {
    public EzTweenMaterialInt(string name, int endValue, bool relative = false)
        : base(name, endValue, relative) {
    }

    protected override int Get() {
        return target.GetInt(Name);
    }

    protected override void Set(int value) {
        target.SetInt(Name, value);
    }
}

public class EzTweenMaterialFloat : EzTweenMaterialProperty<float, FloatTweenable> {
    public EzTweenMaterialFloat(string name, float endValue, bool relative = false)
        : base(name, endValue, relative) {
    }

    protected override float Get() {
        return target.GetFloat(Name);
    }

    protected override void Set(float value) {
        target.SetFloat(Name, value);
    }
}
