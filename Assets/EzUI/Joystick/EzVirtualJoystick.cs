﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

public class EzVirtualJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler {

    public enum AxisOption {
        // Options for which axes to use
        Both, // Use both
        OnlyHorizontal, // Only horizontal
        OnlyVertical // Only vertical
    }

    public bool fixedBed = true;
    public bool alwaysVisible = true;
    public bool alwaysInvisible = false;
    public bool originLowLimit = false;
    public float stickMoveRange = 30f;
    public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
    public string xAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
    public string yAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

    private RectTransform rectTransform;
    private Canvas canvas;
    private Transform bed;
    private Transform stick;
    private Vector3 originBedPosition;
    private float moveRange;
    bool useX; // Toggle for using the x axis
    bool useY; // Toggle for using the Y axis
    CrossPlatformInputManager.VirtualAxis virtualAxisX; // Reference to the joystick in the cross platform input
    CrossPlatformInputManager.VirtualAxis virtualAxisY; // Reference to the joystick in the cross platform input

    void Start() {
        rectTransform = GetComponent<RectTransform>();
        canvas = transform.GetComponentInParent<Canvas>();
        bed = transform.Find("Bed");
        stick = bed.Find("Stick");
        originBedPosition = bed.localPosition;
        moveRange = stickMoveRange;
        if (!alwaysVisible || alwaysInvisible) Hide();
    }

    void OnEnable() {
        CreateVirtualAxes();
    }

    void OnDisable() {
        RemoveVirtualAxes();
    }

    void CreateVirtualAxes() {
        useX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
        useY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);
        if (useX) {
            virtualAxisX = new CrossPlatformInputManager.VirtualAxis(xAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(virtualAxisX);
        }
        if (useY) {
            virtualAxisY = new CrossPlatformInputManager.VirtualAxis(yAxisName);
            CrossPlatformInputManager.RegisterVirtualAxis(virtualAxisY);
        }
    }

    void RemoveVirtualAxes() {
        if (useX) virtualAxisX.Remove();
        if (useY) virtualAxisY.Remove();
    }

    void UpdateVirtualAxes(Vector3 offset) {
        if (useX) virtualAxisX.Update(offset.x);
        if (useY) virtualAxisY.Update(offset.y);
    }

    void Show(float time = 0) {
        iTween.Stop(bed.gameObject);
        iTween.FadeTo(bed.gameObject, 1, time);
    }

    void Hide(float time = 0) {
        iTween.Stop(bed.gameObject);
        iTween.FadeTo(bed.gameObject, 0, time);
    }

    public void OnPointerDown(PointerEventData eventData) {
        Vector2 touchPos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, canvas.worldCamera, out touchPos)) {
            if (!fixedBed) {
                if (!originLowLimit || (touchPos.x > originBedPosition.x && touchPos.y > originBedPosition.y)) {
                    bed.localPosition = touchPos;
                }
            }
            if (!alwaysVisible && !alwaysInvisible) Show(0.2f);
            OnDrag(eventData);
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        OnEndDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData) {
        Vector2 touchPos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, canvas.worldCamera, out touchPos)) {
            Vector3 offset = Vector3.zero;
            if (useX) offset.x = touchPos.x - bed.localPosition.x;
            if (useY) offset.y = touchPos.y - bed.localPosition.y;
            offset = Vector3.ClampMagnitude(offset, moveRange);
            stick.localPosition = offset;
            UpdateVirtualAxes(offset / moveRange);
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        if (!alwaysVisible && !alwaysInvisible) Hide(0.2f);
        else if (!fixedBed) bed.localPosition = originBedPosition;
        stick.localPosition = Vector2.zero;
        UpdateVirtualAxes(Vector3.zero);
    }
}
