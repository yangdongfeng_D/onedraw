﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EzScreenshot {

	public static IEnumerator Capture(string path, UnityAction<string> onFinish = null) {
		yield return Capture(path, new Rect(0, 0, Screen.width, Screen.height), onFinish);
	}

	public static IEnumerator Capture(string path, Rect fromRect, UnityAction<string> onFinish = null) {
		yield return new WaitForEndOfFrame();

		Texture2D tex = new Texture2D((int)fromRect.width, (int)fromRect.height, TextureFormat.RGB24, false);
		tex.ReadPixels(fromRect, 0, 0);
		tex.Apply();
		tex.Save(path);
		if (onFinish != null) {
			onFinish.Invoke(path);
		}
	}

	public static IEnumerator Capture(string path, RectTransform rectTransform, UnityAction<string> onFinish = null) {
		yield return Capture(path, rectTransform.GetScreenRect(), onFinish);
	}

	public static IEnumerator Capture(string path, Rect fromRect, Texture2D toTex, int toX, int toY, UnityAction<string> onFinish = null) {
		yield return new WaitForEndOfFrame();

		toTex.CaptureScreen(fromRect, toX, toY);
		toTex.Save(path);
		if (onFinish != null) {
			onFinish.Invoke(path);
		}
	}

	public static IEnumerator Capture(string path, RectTransform rectTransform, Texture2D toTex, int toX, int toY, UnityAction<string> onFinish = null) {
		yield return Capture(path, rectTransform.GetScreenRect(), toTex, toX, toY, onFinish);
	}

	public static IEnumerator Capture(string path, Rect fromRect, Texture2D toTex, Rect toRect, UnityAction<string> onFinish = null) {
		yield return new WaitForEndOfFrame();

		Texture2D tex = new Texture2D((int)fromRect.width, (int)fromRect.height, TextureFormat.RGB24, false);
		tex.ReadPixels(fromRect, 0, 0);
		tex.Apply();
		toTex.Overlay(tex, (int)toRect.x, (int)toRect.y, (int)toRect.width, (int)toRect.height);
		toTex.Save(path);
		if (onFinish != null) {
			onFinish.Invoke(path);
		}
	}

	public static IEnumerator Capture(string path, RectTransform rectTransform, Texture2D toTex, Rect toRect, UnityAction<string> onFinish = null) {
		yield return Capture(path, rectTransform.GetScreenRect(), toTex, toRect, onFinish);
	}
}
