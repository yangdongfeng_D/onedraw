﻿using UnityEngine;
using System.Collections;

public class ToastManager : MonoBehaviour {

    public static string parentName = "Canvas";

    private static ToastManager _instance;
    public static ToastManager GetInstance() {
        if (_instance == null) {
            _instance = FindObjectOfType<ToastManager>();
            if (_instance == null) {
                _instance = GameObject.Find(parentName).AddComponent<ToastManager>();
            }
        }
        return _instance;
    }

    private Stack toasts = new Stack();

    public Toast Create() {
        Toast toast = null;
        if (toasts.Count > 0) {
            toast = toasts.Pop() as Toast;
            toast.gameObject.SetActive(true);
            toast.transform.SetAsLastSibling();
        } else {
            GameObject toastPrefab = GameManager.instance.prefabs["Toast"];
            if (toastPrefab == null) {
                Debug.LogWarning("Cannot find toast prefab in resources");
            } else { 
                GameObject toastObj = Instantiate(toastPrefab);
                toastObj.name = toastPrefab.name;
                toast = toastObj.GetComponent<Toast>();
                toast.transform.SetParent(transform, false);
            }
        }
        return toast;
    }

    public void Recycle(Toast toast) {
        toasts.Push(toast);
    }

}
