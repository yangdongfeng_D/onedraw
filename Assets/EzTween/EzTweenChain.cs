using UnityEngine;
using System.Collections;


public class EzTweenChain : EzTweenCollection {
	public EzTweenChain() : this(new EzTweenCollectionConfig()) {
	}

	public EzTweenChain(EzTweenCollectionConfig config) : base(config) {
	}

	#region internal Chain management

	private void Append(TweenFlowItem item) {
		// early out for invalid items
		if (item.tween != null && !item.tween.IsValid())
			return;
		
		if (float.IsInfinity(item.duration)) {
			Debug.LogError("adding a Tween with infinite iterations to a TweenChain is not permitted");
			return;
		}

		if (item.tween != null) {
			if (item.tween.reversed != reversed) {
				Debug.LogError("adding a Tween that doesn't match the isReversed property of the TweenChain is not permitted.");
				return;
			}

			// ensure the tween isnt already live
			EzTweenManager.RemoveTween(item.tween);

			// ensure that the item is marked to play.
			item.tween.Play();
		}
		
		flowItems.Add(item);
		
		// update the duration and total duration
		duration += item.duration;
		
		if (loops < 0)
			totalDuration = float.PositiveInfinity;
		else
			totalDuration = duration * loops;
	}
	
	private void Prepend(TweenFlowItem item) {
		// early out for invalid items
		if (item.tween != null && !item.tween.IsValid())
			return;
		
		if (float.IsInfinity(item.duration)) {
			Debug.LogError("adding a Tween with infinite iterations to a TweenChain is not permitted");
			return;
		}

		if (item.tween != null) {
			if (item.tween.reversed != reversed) {
				Debug.LogError("adding a Tween that doesn't match the isReversed property of the TweenChain is not permitted.");
				return;
			}

			// ensure the tween isnt already live
			EzTweenManager.RemoveTween(item.tween);

			// ensure that the item is marked to play.
			item.tween.Play();
		}
		
		// fix all the start times on our previous chains
		foreach (var flowItem in flowItems)
			flowItem.startTime += item.duration;

		flowItems.Insert(0, item);
		
		// update the duration and total duration
		duration += item.duration;

		if (loops < 0)
			totalDuration = float.PositiveInfinity;
		else
			totalDuration = duration * loops;
	}

	#endregion

	#region Chain management

	/// <summary>
	/// appends a Tween at the end of the current flow
	/// </summary>
	public EzTweenChain Append(EzTweenBase tween) {
		var item = new TweenFlowItem(duration, tween);
		Append(item);
		return this;
	}

	
	/// <summary>
	/// appends a delay to the end of the current flow
	/// </summary>
	public EzTweenChain AppendDelay(float delay) {
		var item = new TweenFlowItem(duration, delay);
		Append(item);
		return this;
	}

	
	/// <summary>
	/// adds a Tween to the front of the flow
	/// </summary>
	public EzTweenChain Prepend(EzTweenBase tween) {
		var item = new TweenFlowItem(0, tween);
		Prepend(item);
		return this;
	}

	
	/// <summary>
	/// adds a delay to the front of the flow
	/// </summary>
	public EzTweenChain PrependDelay(float delay) {
		var item = new TweenFlowItem(0, delay);
		Prepend(item);
		return this;
	}

	#endregion

}
