using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EzSplineStraightLineSolver : EzSplineSolver {
	private Dictionary<int, float> segmentStartLocations;
	private Dictionary<int, float> segmentDistances;
	private int currentSegment;

	
	public EzSplineStraightLineSolver(List<Vector3> nodes) {
		this.nodes = nodes;
	}

	#region EzSplineSolver

	public override void BuildPath() {
		// we need at least 3 nodes (more than 1 segment) to bother with building
		if (nodes.Count < 3)
			return;
		
		// we dont care about the first node for distances because they are always t:0 and len:0 and we dont need the first or last for locations
		segmentStartLocations = new Dictionary<int, float>(nodes.Count - 2);
		segmentDistances = new Dictionary<int, float>(nodes.Count - 1);

		for (var i = 0; i < nodes.Count - 1; i++) {
			// calculate the distance to the next node
			var distance = Vector3.Distance(nodes[i], nodes[i + 1]);
			segmentDistances.Add(i, distance);
			pathLength += distance;
		}
		

		// now that we have the total length we can loop back through and calculate the segmentStartLocations
		var accruedRouteLength = 0f;
		for (var i = 0; i < segmentDistances.Count - 1; i++) {
			accruedRouteLength += segmentDistances[i];
			segmentStartLocations.Add(i + 1, accruedRouteLength / pathLength);
		}
	}
	
	public override void ClosePath() {
		// add a node to close the route if necessary
		if (nodes[0] != nodes[nodes.Count - 1])
			nodes.Add(nodes[0]);
	}

	public override Vector3 GetPoint(float t) {
		return GetPointOnPath(t);
	}
	
	public override Vector3 GetPointOnPath(float t) {
		// we need at least 3 nodes (more than 1 segment) to bother using the look up tables. else we just lerp directly from
		// node 1 to node 2
		if (nodes.Count < 3)
			return Vector3.Lerp(nodes[0], nodes[1], t);
		
		
		// which segment are we on?
		currentSegment = 0;
		foreach (var info in segmentStartLocations) {
			if (info.Value < t) {
				currentSegment = info.Key;
				continue;
			}
			
			break;
		}
		
		// now we need to know the total distance travelled in all previous segments so we can subtract it from the total
		// travelled to get exactly how far along the current segment we are
		var totalDistanceTravelled = t * pathLength;
		var i = currentSegment - 1; // we want all the previous segment lengths
		while (i >= 0) {
			totalDistanceTravelled -= segmentDistances[i];
			--i;
		}
		
		return Vector3.Lerp(nodes[currentSegment], nodes[currentSegment + 1], totalDistanceTravelled / segmentDistances[currentSegment]);
	}

	#endregion

}
