﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TargetObject {

    [SerializeField]
    private Component root;

    [SerializeField]
    private string path;

    [SerializeField]
    private string typeName;

    public System.Type type {
        get {
            if (string.IsNullOrEmpty(typeName)) return null;
            return EzUtil.GetType(typeName);
        }
    }

    public Object target {
        get {
            if (string.IsNullOrEmpty(path)) {
                return root;
            }
            string[] names = path.Split('/');
            if (names.Length <= 1) {
                return root;
            }
            Object target = root;
            for (int i = 1; i < names.Length; ++i) {
                if (target == null) break;
                Property<Object> prop = new Property<Object>(names[i], target.GetType());
                target = prop.Get(target);
            }
            return target ?? root;
        }
    }
	
}
