﻿using System;
using System.Reflection;
using UnityEngine;

public class Property<T> : IProperty<T> {

    public string Name { get; protected set; }

    public bool Valid { get { return property != null || field != null; } }

    protected PropertyInfo property = null;
    protected FieldInfo field = null;

    public Property(string name, Type type) {
        Name = name;
        field = type.GetFieldEx(name);
        property = type.GetPropertyEx(name);
    }

    public object GetValue(object target) {
        if (property != null && property.CanRead) {
            return property.GetValue(target, null);
        } else if (field != null) {
            return field.GetValue(target);
        }
        return null;
    }

    public void SetValue(object target, object value) {
        if (property != null && property.CanWrite) {
            property.SetValue(target, value, null);
        } else if (field != null) {
            field.SetValue(target, value);
        }
    }

    public virtual T Get(object target) {
        return (T)GetValue(target);
    }

    public virtual void Set(object target, T value) {
        SetValue(target, value);
    }
}

public class Vector2XProperty : Property<Vector2> {
    public Vector2XProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Vector2 value) {
        Vector2 current = Get(target);
        value.y = current.y;
        base.Set(target, value);
    }
}

public class Vector2YProperty : Property<Vector2> {
    public Vector2YProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Vector2 value) {
        Vector2 current = Get(target);
        value.x = current.x;
        base.Set(target, value);
    }
}

public class Vector3XProperty : Property<Vector3> {
    public Vector3XProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Vector3 value) {
        Vector3 current = Get(target);
        value.y = current.y;
        value.z = current.z;
        base.Set(target, value);
    }
}

public class Vector3YProperty : Property<Vector3> {
    public Vector3YProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Vector3 value) {
        Vector3 current = Get(target);
        value.x = current.x;
        value.z = current.z;
        base.Set(target, value);
    }
}

public class Vector3ZProperty : Property<Vector3> {
    public Vector3ZProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Vector3 value) {
        Vector3 current = Get(target);
        value.x = current.x;
        value.y = current.y;
        base.Set(target, value);
    }
}

public class ColorRProperty : Property<Color> {
    public ColorRProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Color value) {
        Color current = Get(target);
        value.g = current.g;
        value.b = current.b;
        value.a = current.a;
        base.Set(target, value);
    }
}

public class ColorGProperty : Property<Color> {
    public ColorGProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Color value) {
        Color current = Get(target);
        value.r = current.r;
        value.b = current.b;
        value.a = current.a;
        base.Set(target, value);
    }
}

public class ColorBProperty : Property<Color> {
    public ColorBProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Color value) {
        Color current = Get(target);
        value.r = current.r;
        value.g = current.g;
        value.a = current.a;
        base.Set(target, value);
    }
}

public class ColorAProperty : Property<Color> {
    public ColorAProperty(string name, Type type) : base(name, type) { }
    public override void Set(object target, Color value) {
        Color current = Get(target);
        value.r = current.r;
        value.g = current.g;
        value.b = current.b;
        base.Set(target, value);
    }
}
