﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderHandleText : MonoBehaviour {

    public string format = "{0:F0}";

    private Text text;
    private Slider slider;

	// Use this for initialization
	void Start() {
        text = GetComponent<Text>();
        slider = GetComponentInParent<Slider>();
        Update();
    }
	
	// Update is called once per frame
	void Update() {
        text.text = string.Format(format, slider.value);
    }
}
