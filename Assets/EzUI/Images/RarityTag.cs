﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RarityTag : MonoBehaviour {

    public int rarity {
        get { return _rarity; }
        set {
            if (_rarity != value) {
                _rarity = value;
                UpdateUI();
            }
        }
    }
    private int _rarity;

    public Image bg;
    public Image star;
    public Text text;

	// Use this for initialization
	void Start() {
        UpdateUI();
	}

    void UpdateUI() {
        star.color = bg.color = GameManager.instance.rarityColors[rarity];
        text.text = Localization.GetText("Rarity" + rarity);
    }
}
