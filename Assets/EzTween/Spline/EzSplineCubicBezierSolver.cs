using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// nodes should be in the order start, end, control1, control2
/// </summary>
public class EzSplineCubicBezierSolver : EzSplineSolver {
	public EzSplineCubicBezierSolver(List<Vector3> nodes) {
		this.nodes = nodes;
	}

	#region EzSplineSolver

	public override void ClosePath() {
		
	}

	public override Vector3 GetPoint(float t) {
		float d = 1f - t;
		return d * d * d * nodes[0] + 3f * d * d * t * nodes[1] + 3f * d * t * t * nodes[2] + t * t * t * nodes[3];
	}

	public override void DrawGizmos() {
		// draw the control points
		var originalColor = Gizmos.color;
		Gizmos.color = Color.red;
		
		Gizmos.DrawLine(nodes[0], nodes[1]);
		Gizmos.DrawLine(nodes[2], nodes[3]);
		
		Gizmos.color = originalColor;
	}

	#endregion

}
