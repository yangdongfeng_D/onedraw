﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour {

    public Sprite on;
    public Sprite off;

    public Image icon;

    void Start() {
        GetComponent<Button>().onClick.AddListener(OnClick);
        UpdateIcon();
    }

    void OnClick() {
        GameManager.instance.muteSound = !GameManager.instance.muteSound;
        UpdateIcon();
    }

    void UpdateIcon() {
        icon.sprite = GameManager.instance.muteSound ? off : on;
    }
}
