﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WebTexture : MonoBehaviour {

    [System.Serializable]
    public class LoadEvent : UnityEvent<Texture2D> { }

    public string url;
    public bool loadOnAwake = true;
    public Texture2D placeholder;
    public LoadEvent onLoaded;

    void Awake() {
        if (placeholder != null) {
            UpdateTexture(placeholder);
        }
        if (loadOnAwake) {
            Load();
        }
    }

    public void Load() {
        if (string.IsNullOrEmpty(url)) return;
        StartCoroutine(EzImageLoader.LoadTexture(url, (texture) => {
            UpdateTexture(texture);
            if (onLoaded != null) {
                onLoaded.Invoke(texture);
            }
        }));
    }

    public void UpdateTexture(Texture2D texture) {
        Renderer renderer = GetComponent<Renderer>();
        if (renderer != null) {
            renderer.material.mainTexture = texture;
        }
    }
}
